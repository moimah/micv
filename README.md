# MiCV📑 

Aplicación JavaFX de escritorio que permite generar Curriculums en ficheros XML. 
 
 Lee y escribe ficheros xml usando JAXB.
  
 ## Funcionalidades reutilizables: 
   
  - Uso de tablas complejo. 
  - Tablas editables 
  - Uso de listas
  - Enumerados
  - Beans personalizados
  - Uso de inyectores
  - Multi MVC
  - CustomDialogs 
  - Uso de MenuBar
  - Uso de TitledPane 
  - JAXBUtil
  - XMLAdapter (LocalDate)
  - Lector de CSV a ArrayList
 


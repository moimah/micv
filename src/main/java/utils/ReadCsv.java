package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadCsv {

	/**
	 * Recibe un parametro con la direccion del fichero csv Extrae los elementos y
	 * los carga en un un array de Strings el cual devuelve
	 * 
	 * @param uri
	 * @return
	 */
	public ArrayList readFromFile(String uri) {

		ArrayList<String[]> content = new ArrayList<>();

		String file = uri;

		try (BufferedReader br = new BufferedReader(new FileReader(uri, StandardCharsets.ISO_8859_1))) {
			String line = "";
			while ((line = br.readLine()) != null) {
				content.add(line.split(","));
			}
		} catch (Exception e) {
			// Some error logging
		}

		return content;

	}

}

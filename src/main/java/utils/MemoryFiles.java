package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class MemoryFiles {
		
	
	public static void writeFile(File file, String content) {	
	
		
		try {
			
			file.createNewFile();		
			
			FileWriter fileWriter = new FileWriter(file);			
			fileWriter.write(content);
			fileWriter.close();
			
		} catch (Exception e) {			
			e.printStackTrace();
		}		

	}
	
	public static String readFile(File file) {
		
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String content = bufferedReader.readLine();
			bufferedReader.close();
			fileReader.close();
			
			return content; 
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
		
	}
	

}

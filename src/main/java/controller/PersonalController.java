package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.fx.PersonalModel;
import model.pojos.CV;
import model.pojos.Nacionalidad;
import model.pojos.Personal;
import utils.ReadCsv;

public class PersonalController implements Initializable {
	@FXML
	private GridPane root;
	@FXML
	private TextField txtNombre;
	@FXML
	private TextField txtDni;
	@FXML
	private TextField txtApellidos;
	@FXML
	private TextArea txtDireccion;
	@FXML
	private DatePicker dateFechaNacimiento;
	@FXML
	private TextField txtCodigoPostal;
	@FXML
	private TextField txtLocalidad;
	@FXML
	private ComboBox cmbPais;
	@FXML
	private Button btnRemoveNacionalidad;
	@FXML
	private Button btnAddNacionalidad;
	@FXML
	private ListView listNacionalidad;
	
	//Model
	
	private PersonalModel model = new PersonalModel(); 
	
		
	private ArrayList<String> listaNacionalidades = new ArrayList<String>();
	private ObservableList<String> observableNacionalidades;
	
	private ArrayList<String> nacionalidades; //Usado por el combo	
	private ArrayList<String> paises; //usado por el combo
	
	private MainController mainController; 
	
	
	
	public PersonalController() throws IOException {		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/PersonalView.fxml"));
		loader.setController(this);
		loader.load();		
	}
	
	public void injectMainController(MainController mainController) {
		this.mainController = mainController;
	}
		

	@Override
	public void initialize(URL location, ResourceBundle resources) {			
				
		//Bindeos		
		model.txtApellidosProperty().bindBidirectional(txtApellidos.textProperty());		
		model.txtCodigoPostalProperty().bindBidirectional(txtCodigoPostal.textProperty());	
		model.txtDireccionProperty().bindBidirectional(txtDireccion.textProperty());
		
		model.txtDniProperty().bindBidirectional(txtDni.textProperty());
		model.txtLocalidadProperty().bindBidirectional(txtLocalidad.textProperty());
		model.txtNombreProperty().bindBidirectional(txtNombre.textProperty());
		
		model.cmbPaisProperty().bindBidirectional(cmbPais.itemsProperty());
		model.paisSeleccionadoProperty().bind(cmbPais.getSelectionModel().selectedItemProperty()); //Bideo al elemento seleccionado
		
		model.listNacionalidadProperty().bindBidirectional(listNacionalidad.itemsProperty());
		model.nacionalidadSeleccionaProperty().bind(listNacionalidad.getSelectionModel().selectedItemProperty()); //Bindeo al elemento seleccionado
	
		model.dateFechaNacimientoProperty().bindBidirectional(dateFechaNacimiento.valueProperty());
		
		//Listeners propios
		
		model.txtApellidosProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		model.txtCodigoPostalProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		model.txtDniProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		model.txtDireccionProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		model.txtLocalidadProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		model.txtNombreProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		
		model.cmbPaisProperty().addListener((o, ov, nv)->onChangesDetected(nv));	
		model.listNacionalidadProperty().addListener((o, ov, nv)->onChangesDetected(nv)); 
		
		
		model.cmbPaisProperty().addListener((o, ov, nv)->onChangesDetected(nv)); 
		model.dateFechaNacimientoProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		
		model.paisSeleccionadoProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		model.nacionalidadSeleccionaProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		
				
		//Inicializadores
		obtenerNacionalidades();
		obtenerPaises();
		
		
		
	}
	
	
	
	
	/**
	 * Recibe un objeto procente del MainController
	 * y carga los componentes con los atributos del objeto
	 */
	public void cargarElementos(CV cv) {
					
		
		model.setTxtApellidos(cv.getPersonal().getApellidos());
		model.setTxtCodigoPostal(cv.getPersonal().getCodigoPostal());
		model.setTxtDireccion(cv.getPersonal().getDireccion());			
		model.setTxtDni(cv.getPersonal().getDni());
		model.setTxtLocalidad(cv.getPersonal().getLocalidad());
		model.setTxtNombre(cv.getPersonal().getNombre());
		
		cmbPais.getSelectionModel().select(cv.getPersonal().getPais()); //Elemento seleccionado en el combo TODO investigar el bideoBidireccional
		model.setDateFechaNacimiento(cv.getPersonal().getFechaNacimiento());
		
		//Obtener el Array de universidades y pasarlo al ListView		
		try {
			List<Nacionalidad> nacionalidades = cv.getPersonal().getNacionalidades(); //Obtenemos la lista de nacionalidades			
			listaNacionalidades  = new ArrayList<>(nacionalidades.size()); // Convertimos en ArrayList de String
			
			for (int i = 0; i < nacionalidades.size(); i++) {
				listaNacionalidades.add(nacionalidades.get(i).getDenominacion());
			}
			
			
			observableNacionalidades = FXCollections.observableArrayList(listaNacionalidades);
			model.setListNacionalidad(observableNacionalidades);
			
		} catch (Exception e) {
		 //Puede darse una excepcion controlada cuando la lista se encuentra vacia
		}
			
		
		
	}


	/**
	 * Si se detecta un cambio en alguno de los compnentes
	 * se actualiza el objeto CV
	 * @param nv
	 */
	private void onChangesDetected(Object nv) {
		
				
			Personal personal = new Personal();
			
			personal.setDni(model.getTxtDni());
			personal.setApellidos(model.getTxtApellidos());		
			personal.setCodigoPostal(model.getTxtCodigoPostal());
			personal.setDireccion(model.getTxtDireccion());	
			personal.setFechaNacimiento(model.getDateFechaNacimiento());
			personal.setIdentificacion(model.getTxtDni());
			personal.setLocalidad(model.getTxtLocalidad());
			personal.setNombre(model.getTxtNombre());
			personal.setPais(model.getPaisSeleccionado());
			
			
			listaNacionalidades = new ArrayList<String>(); //Se actualiza el arrayList local con los cambios
			
			List<Nacionalidad> listNacionalidades = new ArrayList<Nacionalidad>(); //Nuevo list para CV
			
			try {
			
			for (int i = 0; i < observableNacionalidades.size(); i++) { //Se actualizan con el observabe
				listaNacionalidades.add(observableNacionalidades.get(i));
				listNacionalidades.add(new Nacionalidad(observableNacionalidades.get(i)));
			
			}
			
			personal.setNacionalidades(listNacionalidades);
			
			
		} catch (Exception e) {
			
			//Puede darse una excepcion controlada cuando la lista se encuentra vacia
	
		}
			
			
			
		// Se actualiza el objeto CV del MainController
		
		mainController.getCv().setPersonal(personal);
		
		
		
		 
		 
	
	
		
	}


	// Event Listener on Button[#btnRemoveNacionalidad].onAction
	@FXML
	public void onClickBtnRemoveNacionalidad(ActionEvent event) {	
				
		model.getListNacionalidad().remove(model.getNacionalidadSelecciona());
		
	}
	// Event Listener on Button[#btnAddNacionalidad].onAction
	@FXML
	public void onCllickBtnAddNacionalidad(ActionEvent event) {
		choiceDialog();
	}
		
	/**
	 * Muestra un choiceDialog de nacionalidades
	 * usa el metodo obtenerNacionalidades para cargar
	 * la lista de nacionalidades
	 */
	public void choiceDialog() {
		
		ArrayList<String> nacionalidades = this.nacionalidades;
		
		//Se añaden las nacionalidades al choiceDialog
        ChoiceDialog<String> dialog = new ChoiceDialog<String>(nacionalidades.iterator().next(), nacionalidades); 
        

        dialog.setTitle("MiCV");
        dialog.setHeaderText("Añadir nacionalidad:");
        dialog.setContentText("Seleccione una nacionalidad:");
        
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui
 
        Optional<String> result = dialog.showAndWait();
 
        result.ifPresent(nacionalidad -> { //Si hay resultado cargar elemento en la lista
        	listaNacionalidades.add(nacionalidad);      
        	observableNacionalidades = FXCollections.observableArrayList(listaNacionalidades);
			model.setListNacionalidad(observableNacionalidades);
        	
        });
    }
	
	
	/**
	 * Crea una lista de string a partir de
	 * el metodo readFrom file de la clase ReadCSV
	 * @return
	 */
	
	public void obtenerPaises() {
		
		paises = new ArrayList<String>(); //Lista simple que contendra las nacionalidades
		
		ReadCsv read = new ReadCsv();	//Objeto lector CSV
		
		//Se trasvasa de lista compleja a lista simple la informacion
		try {   
			ArrayList<String[]> listaCompleja =  read.readFromFile("paises.csv");
			
			for(int i = 0; i < listaCompleja.size(); i++) {
				paises.add(listaCompleja.get(i)[0]);				
			}
			
			//Cargar el combo
			ObservableList<String>  observable = FXCollections.observableArrayList(paises);
			model.setCmbPais(observable);
		
			
		} catch (Exception e) {
			// Puede darse una excepción controlada sin el fichero no existe
		}
				
	 
		
	}
	
	
		
	/**
	 * Lee un fichero CSV 7 crea una
	 * lista de string a partir de
	 * el metodo readFrom file de la clase ReadCSV
	 * @return
	 */
	
	public void obtenerNacionalidades() {
		
		nacionalidades = new ArrayList<String>(); //Lista simple que contendra las nacionalidades
		
		ReadCsv read = new ReadCsv();	//Objeto lector CSV
		
		//Se trasvasa de lista compleja a lista simple la informacion
		try {   
			ArrayList<String[]> listaCompleja =  read.readFromFile("nacionalidades.csv");
			
			for(int i = 0; i < listaCompleja.size(); i++) {
				nacionalidades.add(listaCompleja.get(i)[0]);				
			}	
			

			
		} catch (Exception e) {
			// TODO: handle exception
		}
				
	  		
	}
	
	
	public GridPane getRoot() {
		return root;
	}

	
			

}

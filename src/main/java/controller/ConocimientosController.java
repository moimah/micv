package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import controller.dialogs.NuevoConocimientoDialogController;
import controller.dialogs.NuevoIdiomaDialogController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.fx.ConocimientosModel;
import model.fx.beans.ExperienciaBean;
import model.fx.beans.HabilidadesBean;
import model.fx.dialogs.NuevoConocimientoDialogModel;
import model.fx.dialogs.NuevoIdiomaDialogModel;
import model.pojos.CV;
import model.pojos.Experiencia;
import model.pojos.Idioma;
import model.pojos.Nivel;

public class ConocimientosController implements Initializable{
	@FXML
	private GridPane root;
	@FXML
	private TableView<HabilidadesBean> tableConocimientos;
	@FXML
	private TableColumn<HabilidadesBean, String> columnDenominacion;
	@FXML
	private TableColumn<HabilidadesBean, Nivel> columnNivel;
	@FXML
	private Button btnAddConocimiento;
	@FXML
	private Button btnAddIdioma;
	@FXML
	private Button btnEliminar;
	
	private ConocimientosModel model = new ConocimientosModel(); 	
	
	private MainController mainController;
	
	private ArrayList<HabilidadesBean> listaHabilidades = new ArrayList<HabilidadesBean>() ;
	
	private ObservableList<HabilidadesBean> observableHabilidades; 
	
	public ConocimientosController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ConocimientosView.fxml"));
		loader.setController(this);
		loader.load();	
	}
	
	public void injectMainController(MainController mainController) {
		this.mainController = mainController;

	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		model.listaConocimientosProperty().bindBidirectional(tableConocimientos.itemsProperty());		
		columnDenominacion.setCellValueFactory(v -> v.getValue().denominacionProperty());
		columnNivel.setCellValueFactory(v -> v.getValue().nivelProperty());
		
		model.conocimientoSeleccionadoProperty().bind(tableConocimientos.getSelectionModel().selectedItemProperty());
		
		model.listaConocimientosProperty().addListener((o, ov, nv)->onChangesDetected(nv));	//Listener que detecta cambios
		tableConocimientos.editingCellProperty().addListener((o, ov, nv)->onChangesDetected(nv));	//Listener que detecta cambios
		
		
		//Hacer lista editable
		tableConocimientos.setEditable(true);
		columnDenominacion.setCellFactory(TextFieldTableCell.forTableColumn());
		columnNivel.setCellFactory(ComboBoxTableCell.forTableColumn(Nivel.values()));
		
		
		
	}
	

	/**
	 * Reacciona ante los cambios en la lista
	 * Actualiza el objeto principal CV con los nuevos cambios
	 * actualiza el arrayList principal con los cambios
	 * @param nv
	 */
	
	public void onChangesDetected(Object nv) {
		
		observableHabilidades = model.getListaConocimientos();
		
		try {
			
			List<Idioma> habilidades = new ArrayList<Idioma>();
			
			listaHabilidades = new ArrayList<HabilidadesBean>();	
			
			for (int i = 0; i <observableHabilidades.size(); i++) {
				
				Idioma idioma = new Idioma(observableHabilidades.get(i).getDenominacion(), observableHabilidades.get(i).getNivel() , observableHabilidades.get(i).getCertificacion());
				
				listaHabilidades.add(new HabilidadesBean(idioma));
				
				habilidades.add(idioma);
			}
			
			observableHabilidades = FXCollections.observableArrayList(listaHabilidades);
			
			this.mainController.getCv().setHabilidades(habilidades);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		
	}
	
	
	
	/**
	 * Carga un dialogo para añadir nuevo idioma, Devuelve un objeto idioma,
	 * null si se ha cancelado 
	 * @return
	 */
	public Idioma dialogNuevoIdioma() {

		NuevoIdiomaDialogController dialog = new NuevoIdiomaDialogController();
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

	
		Optional<NuevoIdiomaDialogModel> result = dialog.showAndWait();

		if (result.isPresent()) { // Si se han obtenido resultados rellenar objeto telefono
			Idioma habilidad = new Idioma(); 
			habilidad.setDenominacion(result.get().getDenominacion());
			habilidad.setCertificacion(result.get().getCertificacion());
		
			String nivel = result.get().getNivelSeleccionado();
			
			if(nivel!=null) {
				
				switch (nivel) {
				case "BASICO":
					habilidad.setNivel(Nivel.BASICO);		
					break;
				case "MEDIO":
					habilidad.setNivel(Nivel.MEDIO);				
					break;				
				case "AVANZADO":
					habilidad.setNivel(Nivel.AVANZADO);				
					break;
				
				default:
					break;
				}
				
			}
			
			return habilidad;
		}

		return null;

	}
	
	
	/**
	 * Carga un dialogo para añadir nuevo conocimiento, Devuelve un objeto idioma, 
	 * null si se ha cancelado 
	 * @return
	 */
	public Idioma dialogNuevoConocimiento() {

		NuevoConocimientoDialogController dialog = new NuevoConocimientoDialogController();
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

	
		Optional<NuevoConocimientoDialogModel> result = dialog.showAndWait();

		if (result.isPresent()) { // Si se han obtenido resultados rellenar objeto telefono
			Idioma habilidad = new Idioma(); 
			habilidad.setDenominacion(result.get().getDenominacion());
		
			String nivel = result.get().getNivelSeleccionado();
			
			if(nivel!=null) {
				
				switch (nivel) {
				case "BASICO":
					habilidad.setNivel(Nivel.BASICO);				
					break;
				case "MEDIO":
					habilidad.setNivel(Nivel.MEDIO);			
					break;				
				case "AVANZADO":
					habilidad.setNivel(Nivel.AVANZADO);				
					break;
				
				default:
					break;
				}
				
			}
			
			return habilidad;
		}

		return null;

	}
	
	/**
	 * Obtiene un objeto CV desde el MainController,
	 * carga los cambios en el objeto CV principal
	 * rellena la lista con los datos
	 * @param cv
	 */
	
	public void cargarElementos(CV cv) {
		
		try {
			
			List<Idioma> habilidades = cv.getHabilidades(); 
					
			for (int i = 0; i < habilidades.size(); i++) {
				listaHabilidades.add(new HabilidadesBean(habilidades.get(i)));
			}

			observableHabilidades = FXCollections.observableArrayList(listaHabilidades);
			model.setListaConocimientos(observableHabilidades);
		
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	
	}

		
	// Event Listener on Button[#btnAddConocimiento].onAction
	@FXML
	public void onClickAddConocimiento(ActionEvent event) {
		
		 Idioma conocimiento = dialogNuevoConocimiento();
		
		if(conocimiento!=null) {
			listaHabilidades.add(new HabilidadesBean(conocimiento));
			observableHabilidades = FXCollections.observableArrayList(listaHabilidades);
			model.setListaConocimientos(observableHabilidades);
		}
		
		
	}
	// Event Listener on Button[#btnAddIdioma].onAction
	@FXML
	public void onClickAddIdioma(ActionEvent event) {
		
		 Idioma idioma = dialogNuevoIdioma();
			
			if(idioma!=null) {
				listaHabilidades.add(new HabilidadesBean(idioma));
				observableHabilidades = FXCollections.observableArrayList(listaHabilidades);
				model.setListaConocimientos(observableHabilidades);
			}
		
	}
	// Event Listener on Button[#btnEliminar].onAction
	@FXML
	public void onClickEliminar(ActionEvent event) {
		
		

		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Eliminar teléfono");
		alert.setHeaderText("Atención!");
		alert.setContentText("¿Desea eliminar el conocimiento " + model.getConocimientoSeleccionado().getDenominacion()+ "?");
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) alert.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == ButtonType.OK) {
			
			model.listaConocimientosProperty().remove(model.getConocimientoSeleccionado()); //Borra el elemento seleccionado      
			
		} else {
			alert.close();
		}
		
		
	
	}
	public GridPane getRoot() {
		return root;
	}
	public void setRoot(GridPane root) {
		this.root = root;
	}
	
}

package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Optional;
import java.util.ResourceBundle;

import controller.dialogs.NuevaWebDialogController;
import controller.dialogs.NuevoCorreoDialogController;
import controller.dialogs.NuevoTelefonoDialogController;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.fx.ContactoModel;
import model.fx.beans.EmailBean;
import model.fx.beans.TelefonoBean;
import model.fx.beans.WebBean;
import model.fx.dialogs.NuevaWebDialogModel;
import model.fx.dialogs.NuevoCorreoDialogModel;
import model.fx.dialogs.NuevoTelefonoDialogModel;
import model.pojos.CV;
import model.pojos.Contacto;
import model.pojos.Email;
import model.pojos.Telefono;
import model.pojos.TipoTelefono;
import model.pojos.Web;

public class ContactoController implements Initializable {
	@FXML
	private GridPane root;
	@FXML
	private TableView<TelefonoBean> tableTelefonos;
	@FXML
	private TableColumn<TelefonoBean, String> columnNumero;
	@FXML
	private TableColumn<TelefonoBean, TipoTelefono> columnTipo;
	@FXML
	private Button btnAddTelefono;
	@FXML
	private Button btnEliminarTelefono;
	@FXML
	private TableView<EmailBean> tableEmail;
	@FXML
	private TableColumn<EmailBean, String> columnEmail;
	@FXML
	private Button btnAddEmail;
	@FXML
	private Button btnEliminarEmail;
	@FXML
	private TableView<WebBean> tableWeb;
	@FXML
	private TableColumn<WebBean, String> columnUrl;
	@FXML
	private Button btnAddURL;
	@FXML
	private Button btnEliminarURL;
	
	//Others
	
	private ContactoModel model = new ContactoModel();
	
	private MainController mainController; 
	
	private ArrayList<TelefonoBean> listaTelefonos = new ArrayList<TelefonoBean>();
	private ObservableList<TelefonoBean> observableListTelefonos;
	
	private ArrayList<EmailBean> listaEmail = new ArrayList<EmailBean>();
	private ObservableList<EmailBean> observableEmails;
	
	private ArrayList<WebBean> listaWebs = new ArrayList<WebBean>();
	private ObservableList<WebBean> observableWebs;
	
	
	
	public ContactoController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ContactoView.fxml"));
		loader.setController(this);
		loader.load();	
	}
	
	
	public void injectMainController(MainController mainController) {
		this.mainController = mainController; 
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
		//Bindeos		
		
		tableTelefonos.itemsProperty().bindBidirectional(model.telefonosProperty());		
		columnNumero.setCellValueFactory(v -> v.getValue().numeroProperty()); //Enlace de columna
		columnTipo.setCellValueFactory(v -> v.getValue().tipoTelefonoProperty());//Enlace de columna	
		model.telefonoSeleccionadoProperty().bind(tableTelefonos.getSelectionModel().selectedItemProperty()); //Bindeo al elemento seleccionado
		
		
		tableEmail.itemsProperty().bindBidirectional(model.emailsProperty()); 
		columnEmail.setCellValueFactory(v -> v.getValue().direccionProperty());
		model.emailSeleccionadoProperty().bind(tableEmail.getSelectionModel().selectedItemProperty());
		
		tableWeb.itemsProperty().bindBidirectional(model.websProperty());
		columnUrl.setCellValueFactory(v -> v.getValue().urlProperty()); //Enlace de columna
		model.webSeleccionadaProperty().bind(tableWeb.getSelectionModel().selectedItemProperty()); //Bindeo al emenento seleccionado
		
		//Listeners
		 model.telefonosProperty().addListener((o, ov, nv)->onChangesDetected(nv));	
		 model.emailsProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		 model.websProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		
		 
		 tableTelefonos.editingCellProperty().addListener((o, ov, nv)->onChangesDetected(nv)); //Listeners que actua con cambios en al tabla
		 tableEmail.editingCellProperty().addListener((o, ov, nv)->onChangesDetected(nv)); //Listeners que actua con cambios en al tabla
		 tableWeb.editingCellProperty().addListener((o, ov, nv)->onChangesDetected(nv)); //Listeners que actua con cambios en al tabla
		 
		 
		 //Tablas editables
		 tableTelefonos.setEditable(true);
		 columnNumero.setCellFactory(TextFieldTableCell.forTableColumn());
		 columnTipo.setCellFactory(ComboBoxTableCell.forTableColumn(TipoTelefono.values()));
		 
		 
	     tableEmail.setEditable(true);
	     columnEmail.setCellFactory(TextFieldTableCell.forTableColumn());
	     
	     tableWeb.setEditable(true);
	     columnUrl.setCellFactory(TextFieldTableCell.forTableColumn());				 
		 
	}
	
	public void cargarElementos(CV cv) {
		
		
				//Actualizar Telefonos
				try {
					
					List<Telefono> telefonos = cv.getContacto().getTelefonos();				
					
					listaTelefonos = new ArrayList<TelefonoBean>();	//Se inicializa la lista	
					
					for(int i=0; i<telefonos.size();i++) {	
						listaTelefonos.add(new TelefonoBean(telefonos.get(i)));						
					}
					observableListTelefonos = FXCollections.observableArrayList(listaTelefonos);
					model.setTelefonos(observableListTelefonos);
					
				} catch (Exception e) {
					// No se han producido cambios
				}
				
				//Actualizar emails
				try {
					
					List<Email> emails = cv.getContacto().getEmails();
										
					listaEmail = new ArrayList<EmailBean>();
					
					for (int i = 0; i < emails.size(); i++) {
						listaEmail.add(new EmailBean(emails.get(i)));
												
					}
					observableEmails = FXCollections.observableArrayList(listaEmail);
					model.setEmails(observableEmails);
					
				} catch (Exception e) {
					// No se han producido cambios
				}
				
				//Atualizar  Webs
				try {
					List<Web> webs = cv.getContacto().getWebs();
					
					listaWebs = new ArrayList<WebBean>();
					for (int i = 0; i < webs.size(); i++) {
						listaWebs.add(new WebBean(webs.get(i)));
					}
					observableWebs = FXCollections.observableArrayList(listaWebs);
					model.setWebs(observableWebs);
					
				} catch (Exception e) {
					// El cambio no ha sido en la lista de webs
				}
				
		
	}
	
	/**
	 * Muestra un dialogo personalizado y carga un objeto
	 * telefono con los resultados obtenidos a través del dialogo
	 * @return
	 */
	
	public Telefono dialogNuevoTelefono() {		
		
		NuevoTelefonoDialogController dialog = new NuevoTelefonoDialogController(); // Se crea un nuevo dialogo		
			
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui
		
		Optional<NuevoTelefonoDialogModel> result = dialog.showAndWait(); //Se muestra y se carga en variable resultado		
		
		if (result.isPresent()) { //Si se han obtenido resultados rellenar objeto telefonoç
			Telefono telefono = new Telefono();
			telefono.setNumero(result.get().getNumero());	
			
			if(result.get().getSeleccionado().contains("DOMICILIO")) {
				telefono.setTipo(TipoTelefono.DOMICILIO);
			}else if(result.get().getSeleccionado().contains("MÓVIL")) {
				telefono.setTipo(TipoTelefono.MOVIL);
			}
			return telefono;		
		}
		
		return null;
		
	}
	
	
	/**
	 * Muestra un dialogo personalizado y carga un objeto
	 * email con los resultados obtenidos a través del dialogo
	 * @return
	 */
	
	public Email dialogNuevoEmail() {
		
		NuevoCorreoDialogController dialog = new NuevoCorreoDialogController(); // Se crea un nuevo dialogo
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui
		
		Optional<NuevoCorreoDialogModel> result = dialog.showAndWait(); //Se muestra y se carga en variable resultado
		
		
		if (result.isPresent()) { //Si se han obtenido resultados rellenar objeto telefono
			Email email = new Email();
			email.setDireccion(result.get().getEmail());
			return email;
		}
		
		return null;
				
	}
	
	/**
	 * Muestra un dialogo personalizado y carga un objeto
	 * email con los resultados obtenidos a través del dialogo
	 * @return
	 */	
	public Web dialogNuevaWeb() {
		
		NuevaWebDialogController dialog = new 	NuevaWebDialogController(); // Se crea un nuevo dialogo
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui
		
		Optional<NuevaWebDialogModel> result = dialog.showAndWait(); //Se muestra y se carga en variable resultado
				
		if (result.isPresent()) { //Si se han obtenido resultados rellenar objeto telefono		
			Web web = new Web(); 
			web.setUrl(result.get().getUrl());
			return web;
		}
		
		return null; 
		
	}
	

	/**
	 * Reacciona ante algun cambio en las tablas actualizando
	 * el objeto principal contacto, y el ArrayList global con
	 * los cambios recibidos en el observableList	 
	 * @param nv
	 */
	public void onChangesDetected(Object nv) {		
		
				
		observableListTelefonos = model.getTelefonos();
		observableEmails = model.getEmails();
		observableWebs = model.getWebs();
	
		Contacto contacto = new Contacto();
		
		//Actualizar Cambios en objeto principal
		try {
			
			List<Telefono> telefonos =   new ArrayList<Telefono>();			
			listaTelefonos = new ArrayList<TelefonoBean>();
			
			

			for (int i = 0; i < observableListTelefonos.size(); i++) {
				Telefono telefono = new Telefono(observableListTelefonos.get(i).getNumero(),observableListTelefonos.get(i).getTipoTelefono());						
				telefonos.add(telefono);
				listaTelefonos.add(new TelefonoBean(telefono));
			}
						
			observableListTelefonos = FXCollections.observableArrayList(listaTelefonos);				
							
			contacto.setTelefonos(telefonos); //Se cargan los cambios en el objeto contacto			
		
			this.mainController.getCv().setContacto(contacto);
	
		} catch (Exception e) {
			// El cambio no ha sido en la lista telefonos
		}
		
		//Actualizar Cambios en objeto principal
		try {	
			
			List<Email> emails = new ArrayList<Email>(); 			
			listaEmail = new ArrayList<EmailBean>(); 
			
			
			for (int i = 0; i < observableEmails.size(); i++) {
				Email email = new Email(observableEmails.get(i).getDireccion());
				emails.add(email);
				listaEmail.add(observableEmails.get(i));				
			}
			
			observableEmails = FXCollections.observableArrayList(listaEmail);
			
			contacto.setEmails(emails); //Se cargan los cambios en el objeto contacto
			
			this.mainController.getCv().setContacto(contacto); //Se cargan los cambios en el cv global
			
			
		} catch (Exception e) {
			// El cambio no ha sido en la lista de emails
		}
		
		//Actualizar cambios en el objeto principal
		try {
			
			List<Web> webs = new ArrayList<Web>();			
			listaWebs = new ArrayList<WebBean>();
			
			for(int i = 0; i <observableWebs.size(); i++) {
				Web web = new Web(observableWebs.get(i).getUrl());
				webs.add(web); 
				listaWebs.add(observableWebs.get(i));				
			}
			
			observableWebs = FXCollections.observableArrayList(listaWebs);
			
			contacto.setWebs(webs);; //Se cargan los cambios en el objeto contacto 
			
			this.mainController.getCv().setContacto(contacto);
			
			
		} catch (Exception e) {
			// El cambio no ha sido en la lista de webs
		}
				
	}

	@FXML
	public void onClickBtnAddTelefono(ActionEvent event) {			
		
			Telefono telefono = dialogNuevoTelefono();
			
			if(telefono!=null) {
				//Añadir a la lista un nuevo TelefonoBean
				TelefonoBean telefonoBean = new TelefonoBean(telefono);
				listaTelefonos.add(telefonoBean);
				observableListTelefonos = FXCollections.observableArrayList(listaTelefonos);		
				model.setTelefonos(observableListTelefonos);
			}
			
			
					
	}
	
	@FXML
	public void onClickBtnEliminarTelefono(ActionEvent event) {
		
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Eliminar teléfono");
		alert.setHeaderText("Atención!");
		alert.setContentText("¿Desea eliminar el teléfono " + model.getTelefonoSeleccionado().getTelefono().getNumero()+ "?");
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) alert.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == ButtonType.OK) {
			
			model.getTelefonos().remove(model.getTelefonoSeleccionado());	
	       
			
		} else {
			alert.close();
		}

   
}
		

	// Event Listener on Button[#btnAddEmail].onAction
	@FXML
	public void onClickBtnAddEmail(ActionEvent event) {
		
		
		Email email = dialogNuevoEmail(); 
		
		if(email!=null) { 
			if(email.getDireccion().contains("@")) {
				//Añadir a la lista un nuevo EmailBean
				EmailBean emailBean  = new EmailBean(email);
				listaEmail.add(emailBean);
				observableEmails = FXCollections.observableArrayList(listaEmail);
				model.setEmails(observableEmails);	
			}else {
				//TODO alert de mal formato en email
			}
			
		}
			
	}
	
	// Event Listener on Button[#btnEliminarEmail].onAction
	@FXML
	public void onClickBtnEliminarEmail(ActionEvent event) {
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Eliminar teléfono");
		alert.setHeaderText("Atención!");
		alert.setContentText("¿Desea eliminar el E-mail " + model.getEmailSeleccionado().getEmail().getDireccion()+ "?");
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) alert.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == ButtonType.OK) {
			
			model.getEmails().remove(model.getEmailSeleccionado()); //borra el elemento seleccionado	       
			
		} else {
			alert.close();
		}
		
		
		
	}
	
	// Event Listener on Button[#btnAddURL].onAction
	@FXML
	public void onClickBtnAddWeb(ActionEvent event) {
		
		Web web = dialogNuevaWeb();
		
		if(web!=null) {
			//Añadir un elemento webBean a la lista
			WebBean webBean = new WebBean(web);
			listaWebs.add(webBean);
			observableWebs = FXCollections.observableArrayList(listaWebs);
			model.setWebs(observableWebs);
		}
				
		
	}
	// Event Listener on Button[#btnEliminarURL].onAction
	@FXML
	public void onClickBtnEliminarWeb(ActionEvent event) {
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Eliminar teléfono");
		alert.setHeaderText("Atención!");
		alert.setContentText("¿Desea eliminar la web " + model.getWebSeleccionada().getWeb().getUrl()+ "?");
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) alert.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == ButtonType.OK) {
			
			model.getWebs().remove(model.getWebSeleccionada()); //Borra el elemento seleccionado      
			
		} else {
			alert.close();
		}
		
		
	}
	


	public GridPane getRoot() {
		return root;
	}

	
	
}

package controller.dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import model.fx.dialogs.FechaDialogModel;


public class FechaDialogController extends Dialog<FechaDialogModel> implements Initializable {
	@FXML
	private GridPane view;
	
	@FXML
	private DatePicker dateFecha;
	
	
	private FechaDialogModel model = new FechaDialogModel(); 
	
	private ButtonType CREAR_BUTTON_TYPE = new ButtonType("Crear", ButtonData.OK_DONE);
	
	
	
	public FechaDialogController() {
		super();
		setTitle("Nueva fecha");
		//setHeaderText("Introduzca el nuevo número de teléfono.");
		//setGraphic(new ImageView(getClass().getResource("/images/telefono.png").toString()));
		getDialogPane().getButtonTypes().addAll(
				CREAR_BUTTON_TYPE, // botón personalizado
				ButtonType.CANCEL
			);
		getDialogPane().setContent(loadContent("/fxml/FechaDialog.fxml"));
		setResultConverter(dialogButton -> {
		    if (dialogButton.getButtonData() == ButtonData.OK_DONE) {
		    	
		    	FechaDialogModel nuevaFechaForDialog = new FechaDialogModel();
		    	nuevaFechaForDialog.setFecha(model.getFecha());
		    	
		        return nuevaFechaForDialog; 
		    }
		    return null;
		});	
		
	}
	
	private Node loadContent(String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
			loader.setController(this);
			return loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		model.fechaProperty().bindBidirectional(dateFecha.valueProperty());
	
		
		Node crearButton = getDialogPane().lookupButton(CREAR_BUTTON_TYPE); //El boton se activa si los campos no están vacios
		crearButton.disableProperty().bind(
				model.fechaProperty().isNull());				
				
		
	}

	public FechaDialogModel getModel() {
		return model;
	}

	
	
		
}

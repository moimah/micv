package controller.dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import model.fx.dialogs.NuevaWebDialogModel;
import model.fx.dialogs.NuevoCorreoDialogModel;

public class NuevaWebDialogController extends Dialog<NuevaWebDialogModel> implements Initializable {
	
	@FXML
	private BorderPane view;

	@FXML
	private TextField txtUrl;
	
	//model
	
	NuevaWebDialogModel model = new NuevaWebDialogModel();
	
	private ButtonType ACEPTAR_BUTTON_TYPE = new ButtonType("Aceptar", ButtonData.OK_DONE);
	
	public NuevaWebDialogController() {
		
		super();
		setTitle("Nueva web");
		setHeaderText("Crear una dirección web.");
		setGraphic(new ImageView(getClass().getResource("/images/www.png").toString()));
		getDialogPane().getButtonTypes().addAll(
				ACEPTAR_BUTTON_TYPE, // botón personalizado
				ButtonType.CANCEL
			);
		getDialogPane().setContent(loadContent("/fxml/NuevaWebDialog.fxml"));
		setResultConverter(dialogButton -> {
		    if (dialogButton.getButtonData() == ButtonData.OK_DONE) {	 
		    	
		    	NuevaWebDialogModel nuevaWebForDialog = new NuevaWebDialogModel();
		    	nuevaWebForDialog.setUrl(model.getUrl());		    
		        return nuevaWebForDialog; 
		    }
		    return null;
		});
		
	}
	
	private Node loadContent(String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
			loader.setController(this);
			return loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		// Bindeos
		model.urlProperty().bindBidirectional(txtUrl.textProperty());

		Node crearButton = getDialogPane().lookupButton(ACEPTAR_BUTTON_TYPE);
		crearButton.disableProperty().bind(model.urlProperty().isEmpty()); // El boton se activa si los campos no están vacios
																			

	}

}

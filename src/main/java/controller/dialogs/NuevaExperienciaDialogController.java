package controller.dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.fx.dialogs.NuevaExperienciaDialogModel;
import model.fx.dialogs.NuevoTelefonoDialogModel;
import model.fx.dialogs.NuevoTituloDialogModel;

public class NuevaExperienciaDialogController extends Dialog<NuevaExperienciaDialogModel> implements Initializable {
	@FXML
	private GridPane view;
	@FXML
	private TextField txtDenominacion;
	@FXML
	private TextField txtEmpleador;
	@FXML
	private DatePicker dateDesde;
	@FXML
	private DatePicker dateHasta;
	
	private NuevaExperienciaDialogModel model = new NuevaExperienciaDialogModel();
	
	private ButtonType CREAR_BUTTON_TYPE = new ButtonType("Crear", ButtonData.OK_DONE);
	
	
	
	public NuevaExperienciaDialogController() {
		super();
		setTitle("Nueva experiencia");
		//setHeaderText("Introduzca el nuevo número de teléfono.");
		//setGraphic(new ImageView(getClass().getResource("/images/telefono.png").toString()));
		getDialogPane().getButtonTypes().addAll(
				CREAR_BUTTON_TYPE, // botón personalizado
				ButtonType.CANCEL
			);
		getDialogPane().setContent(loadContent("/fxml/NuevaExperienciaDialog.fxml"));
		setResultConverter(dialogButton -> {
		    if (dialogButton.getButtonData() == ButtonData.OK_DONE) {
		    	
		    	NuevaExperienciaDialogModel nuevaExperienciaForDialog = new NuevaExperienciaDialogModel();
		    	nuevaExperienciaForDialog.setDenominacion(model.getDenominacion());		    	
		    	nuevaExperienciaForDialog.setEmpleador(model.getEmpleador());
		    	nuevaExperienciaForDialog.setDesde(model.getDesde());
		    	nuevaExperienciaForDialog.setHasta(model.getHasta());
		    	
		    	return nuevaExperienciaForDialog;
		       
		    }
		    return null;
		});	
		
	}
	
	private Node loadContent(String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
			loader.setController(this);
			return loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		model.denominacionProperty().bindBidirectional(txtDenominacion.textProperty());
		model.empleadorProperty().bindBidirectional(txtEmpleador.textProperty());
		model.desdeProperty().bindBidirectional(dateHasta.valueProperty());
		model.hastaProperty().bindBidirectional(dateHasta.valueProperty());
		
		Node crearButton = getDialogPane().lookupButton(CREAR_BUTTON_TYPE); //El boton se activa si los campos no están vacios
		crearButton.disableProperty().bind(
				model.denominacionProperty().isEmpty()
				.or(model.empleadorProperty().isEmpty())
				.or(model.denominacionProperty().isEmpty())
				.or(model.desdeProperty().isNull())
				.or(model.hastaProperty().isNull())
				);
		
	}

	
	
}

package controller.dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.BorderPane;
import model.fx.dialogs.NuevoCorreoDialogModel;
import model.fx.dialogs.NuevoTelefonoDialogModel;

public class NuevoCorreoDialogController extends Dialog<NuevoCorreoDialogModel> implements Initializable {
	
	@FXML
	private BorderPane view;

	@FXML
	private TextField txtEmail;
	
	//Model
	private NuevoCorreoDialogModel model = new NuevoCorreoDialogModel();

	private ButtonType ACEPTAR_BUTTON_TYPE = new ButtonType("Aceptar", ButtonData.OK_DONE);
	
	public NuevoCorreoDialogController() {
		super();
		setTitle("Nuevo E-mail");
		setHeaderText("Crear una nueva dirección de correo.");
		setGraphic(new ImageView(getClass().getResource("/images/email.png").toString()));
		getDialogPane().getButtonTypes().addAll(
				ACEPTAR_BUTTON_TYPE, // botón personalizado
				ButtonType.CANCEL
			);
		getDialogPane().setContent(loadContent("/fxml/NuevoCorreoDialog.fxml"));
		setResultConverter(dialogButton -> {
		    if (dialogButton.getButtonData() == ButtonData.OK_DONE) {	    
		    			    	NuevoCorreoDialogModel nuevoCorreoForDialog = new NuevoCorreoDialogModel();
		    	nuevoCorreoForDialog.setEmail(model.getEmail());
		        return nuevoCorreoForDialog;
		    }
		    return null;
		});
	}
	
	private Node loadContent(String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
			loader.setController(this);
			return loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		//Bindeos
		model.emailProperty().bindBidirectional(txtEmail.textProperty());
		
		Node crearButton = getDialogPane().lookupButton(ACEPTAR_BUTTON_TYPE);
		crearButton.disableProperty().bind( model.emailProperty().isEmpty());	//El boton se activa si los campos no están vacios				
				
	}

}

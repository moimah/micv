package controller.dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.fx.dialogs.NuevoTelefonoDialogModel;
import model.fx.dialogs.NuevoTituloDialogModel;

public class NuevoTituloDialogController extends Dialog<NuevoTituloDialogModel> implements Initializable {
	@FXML
	private GridPane view;
	@FXML
	private TextField txtDenominacion;
	@FXML
	private TextField txtOrganizador;
	@FXML
	private DatePicker dateDesde;
	@FXML
	private DatePicker dateHasta;
	
	private NuevoTituloDialogModel model = new NuevoTituloDialogModel(); 
	
	private ButtonType CREAR_BUTTON_TYPE = new ButtonType("Crear", ButtonData.OK_DONE);
	
	
	
	public NuevoTituloDialogController() {
		super();
		setTitle("Nuevo título");
		//setHeaderText("Introduzca el nuevo número de teléfono.");
		//setGraphic(new ImageView(getClass().getResource("/images/telefono.png").toString()));
		getDialogPane().getButtonTypes().addAll(
				CREAR_BUTTON_TYPE, // botón personalizado
				ButtonType.CANCEL
			);
		getDialogPane().setContent(loadContent("/fxml/NuevoTituloDialog.fxml"));
		setResultConverter(dialogButton -> {
		    if (dialogButton.getButtonData() == ButtonData.OK_DONE) {
		    	
		    	NuevoTituloDialogModel nuevoTituloForDialog = new NuevoTituloDialogModel();
		    	nuevoTituloForDialog.setDenominacion(model.getDenominacion());
		    	nuevoTituloForDialog.setOrganizador(model.getOrganizador());
		    	nuevoTituloForDialog.setDesde(model.getDesde());
		    	nuevoTituloForDialog.setHasta(model.getHasta());

		        return nuevoTituloForDialog; 
		    }
		    return null;
		});	
		
	}
	
	private Node loadContent(String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
			loader.setController(this);
			return loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		model.denominacionProperty().bindBidirectional(txtDenominacion.textProperty());
		model.organizadorProperty().bindBidirectional(txtOrganizador.textProperty());
		model.desdeProperty().bindBidirectional(dateHasta.valueProperty());
		model.hastaProperty().bindBidirectional(dateHasta.valueProperty());
		
		Node crearButton = getDialogPane().lookupButton(CREAR_BUTTON_TYPE); //El boton se activa si los campos no están vacios
		crearButton.disableProperty().bind(
				model.denominacionProperty().isEmpty()
				.or(model.organizadorProperty().isEmpty())
				.or(model.denominacionProperty().isEmpty())
				.or(model.desdeProperty().isNull())
				.or(model.hastaProperty().isNull())
				);
		
	}

		
}

package controller.dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import model.fx.beans.TelefonoBean;
import model.fx.dialogs.NuevoTelefonoDialogModel;
import model.pojos.TipoTelefono;

public class NuevoTelefonoDialogController extends Dialog<NuevoTelefonoDialogModel> implements Initializable {
		
	@FXML
	private BorderPane view;

	@FXML
	private TextField txtTelefono;

	@FXML
	private ComboBox cmbTipo;
	

	private NuevoTelefonoDialogModel model = new NuevoTelefonoDialogModel();
	
	private ButtonType ADD_BUTTON_TYPE = new ButtonType("Añadir", ButtonData.OK_DONE);
	
	
	public NuevoTelefonoDialogController() {
		super();
		setTitle("Nuevo Telefono");		
		setHeaderText("Introduzca el nuevo número de teléfono.");
		setGraphic(new ImageView(getClass().getResource("/images/telefono.png").toString()));
		getDialogPane().getButtonTypes().addAll(
				ADD_BUTTON_TYPE, // botón personalizado
				ButtonType.CANCEL
			);
		getDialogPane().setContent(loadContent("/fxml/NuevoTelefonoDialog.fxml"));
		setResultConverter(dialogButton -> {
		    if (dialogButton.getButtonData() == ButtonData.OK_DONE) {
		    	
		    	NuevoTelefonoDialogModel nuevoTelefonoForDialog = new NuevoTelefonoDialogModel();
		    	nuevoTelefonoForDialog.setNumero(model.getNumero());
		    	nuevoTelefonoForDialog.setTipoTelefono(model.getTipoTelefono());	
		    	nuevoTelefonoForDialog.setSeleccionado(model.getSeleccionado()); //En pruebas
		        return nuevoTelefonoForDialog;
		    }
		    return null;
		});	
		
	}
	
	private Node loadContent(String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
			loader.setController(this);
			return loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {

		//Bindeos
		model.numeroProperty().bindBidirectional(txtTelefono.textProperty());
		model.tipoTelefonoProperty().bindBidirectional(cmbTipo.itemsProperty());
		model.seleccionadoProperty().bind(cmbTipo.getSelectionModel().selectedItemProperty()); //Bindeo al elemento seleccionado

		// Añadimos los tipos de teléfono a un observableList y lo mostramos
		ArrayList<String> tipos = new ArrayList<String>();
		tipos.add("DOMICILIO");
		tipos.add("MÓVIL");
		ObservableList<String> list = FXCollections.observableArrayList(tipos);
		model.setTipoTelefono(list);
		
		Node crearButton = getDialogPane().lookupButton(ADD_BUTTON_TYPE); //El boton se activa si los campos no están vacios
		crearButton.disableProperty().bind(
				model.numeroProperty().isEmpty()
				.or(model.seleccionadoProperty().isEmpty())				
				);

	}



	public BorderPane getView() {
		return view;
	}



	public void setView(BorderPane view) {
		this.view = view;
	}
	
	

}

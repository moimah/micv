package controller.dialogs;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.ButtonBar.ButtonData;
import model.fx.dialogs.NuevoIdiomaDialogModel;

public class NuevoIdiomaDialogController extends Dialog<NuevoIdiomaDialogModel> implements Initializable {
	@FXML
	private TextField txtDenominacion;
	@FXML
	private ComboBox cmbNivel;
	@FXML
	private Button btnRemoveNivel;
	@FXML
	private TextField txtCertificacion;
	
	private NuevoIdiomaDialogModel model = new NuevoIdiomaDialogModel(); 
	
    private ButtonType CREAR_BUTTON_TYPE = new ButtonType("Crear", ButtonData.OK_DONE);
	
	private ArrayList<String> listaNiveles = new ArrayList<String>(); 
	private ObservableList<String> observableNiveles; 

	
	public NuevoIdiomaDialogController() {

		 super();
			setTitle("Nuevo conocimiento");
			//setHeaderText("Introduzca el nuevo número de teléfono.");
			//setGraphic(new ImageView(getClass().getResource("/images/telefono.png").toString()));
			getDialogPane().getButtonTypes().addAll(
					CREAR_BUTTON_TYPE, // botón personalizado
					ButtonType.CANCEL
				);
			getDialogPane().setContent(loadContent("/fxml/NuevoIdiomaDialog.fxml"));
			setResultConverter(dialogButton -> {
			    if (dialogButton.getButtonData() == ButtonData.OK_DONE) {
			    	
			    	NuevoIdiomaDialogModel nuevoIdiomaForDialog = new NuevoIdiomaDialogModel();
			    	nuevoIdiomaForDialog.setDenominacion(model.getDenominacion());
			    	nuevoIdiomaForDialog.setCertificacion(model.getCertificacion());
			    	nuevoIdiomaForDialog.setNivelSeleccionado(model.getNivelSeleccionado());			    			 
						    	
			    	return nuevoIdiomaForDialog;
			       
			    }
			    return null;
			});	
	}
	
	private Node loadContent(String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
			loader.setController(this);
			return loader.load();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		// Bindeos
		model.denominacionProperty().bindBidirectional(txtDenominacion.textProperty());
		model.certificacionProperty().bindBidirectional(txtCertificacion.textProperty());
		model.nivelProperty().bindBidirectional(cmbNivel.itemsProperty());
		model.nivelSeleccionadoProperty().bind(cmbNivel.getSelectionModel().selectedItemProperty()); // Bindeo al elememnto seleccionado
																										
		// Inicializar lista con los niveles
		listaNiveles.add("BASICO");
		listaNiveles.add("MEDIO");
		listaNiveles.add("AVANZADO");

		observableNiveles = FXCollections.observableArrayList(listaNiveles);
		model.setNivel(observableNiveles);

		// Inicializar el nodo

		Node crearButton = getDialogPane().lookupButton(CREAR_BUTTON_TYPE); // El boton se activa si los campos no están vacios																			
		crearButton.disableProperty().bind(model.denominacionProperty().isEmpty()
				.or(model.certificacionProperty().isEmpty()));
		
		
	}
	
	// Event Listener on Button[#btnRemoveNivel].onAction
		@FXML
		public void onClickBtnRemoveNivel(ActionEvent event) {
			model.nivelProperty().clear();
		}
}

package controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import controller.dialogs.NuevaExperienciaDialogController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.converter.LocalDateStringConverter;
import model.fx.ExperienciaModel;
import model.fx.beans.ExperienciaBean;
import model.fx.beans.FormacionBean;
import model.fx.dialogs.NuevaExperienciaDialogModel;
import model.fx.dialogs.NuevoTituloDialogModel;
import model.pojos.CV;
import model.pojos.Experiencia;
import model.pojos.Idioma;
import model.pojos.Titulo;

public class ExperienciaController implements Initializable {
	@FXML
	private GridPane root;
	@FXML
	private TableView<ExperienciaBean> tableExperiencia;
	@FXML
	private TableColumn<ExperienciaBean, LocalDate> columnDesde;
	@FXML
	private TableColumn<ExperienciaBean, LocalDate> columnHasta;
	@FXML
	private TableColumn<ExperienciaBean, String> columnDenominacion;
	@FXML
	private TableColumn<ExperienciaBean, String> columnEmpleador;
	@FXML
	private Button btnAddExperiencia;
	@FXML
	private Button btnEliminarExperiencia;
	
	private ExperienciaModel model = new ExperienciaModel(); 
	
	private MainController mainController;
	
	private ArrayList<ExperienciaBean> listaExperiencias = new ArrayList<ExperienciaBean>(); 
	private ObservableList<ExperienciaBean> observableExperiencia; 
	
	public ExperienciaController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ExperienciaView.fxml"));
		loader.setController(this);
		loader.load();		
	}
	
	public void injectMainController(MainController mainController) {
		this.mainController = mainController; 
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		model.experienciaListProperty().bindBidirectional(tableExperiencia.itemsProperty());
		columnHasta.setCellValueFactory(v -> v.getValue().hastaProperty());
		columnDesde.setCellValueFactory(v -> v.getValue().desdeProperty());
		columnDenominacion.setCellValueFactory(v -> v.getValue().denominacionProperty());
		columnEmpleador.setCellValueFactory(v -> v.getValue().empleadorProperty());		
		model.experienciaSeleccionadaProperty().bind(tableExperiencia.getSelectionModel().selectedItemProperty()); //Bindeo al elemento seleccionado
		
		model.experienciaListProperty().addListener((o, ov, nv)->onChangesDetected(nv));	//Listener que detecta cambios
		tableExperiencia.editingCellProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		
		tableExperiencia.setEditable(true);
		columnDesde.setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateStringConverter()));
		columnHasta.setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateStringConverter()));
		columnDenominacion.setCellFactory(TextFieldTableCell.forTableColumn());
		columnEmpleador.setCellFactory(TextFieldTableCell.forTableColumn());
		
		
	}
	
	/**
	 * Carga un dialogo para añadir nueva experiencia Devuelve un objeto experiencia,
	 * null si se ha cancelado 
	 * @return
	 */
	public Experiencia dialogNuevaExperiencia() {

		NuevaExperienciaDialogController dialog = new NuevaExperienciaDialogController();
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		
		Optional<NuevaExperienciaDialogModel> result = dialog.showAndWait();

		if (result.isPresent()) { // Si se han obtenido resultados rellenar objeto telefono
			Experiencia experiencia = new Experiencia();
			experiencia.setDenominacion(result.get().getDenominacion());
			experiencia.setEmpleador(result.get().getEmpleador());
			experiencia.setDesde(result.get().getDesde());
			experiencia.setHasta(result.get().getHasta());

			return experiencia;
		}

		return null;

	}
	
	/**
	 * Reacciona ante los cambios en la lista
	 * Actualiza el objeto principal CV con los nuevos cambios
	 * actualiza el arrayList principal con los cambios
	 * @param nv
	 */
	
	public void onChangesDetected(Object nv) {
		
		observableExperiencia = model.getExperienciaList();
		
		try {
			List<Experiencia> experiencias = new ArrayList<Experiencia>(); 
			
			listaExperiencias = new ArrayList<ExperienciaBean>();	
			
			for (int i = 0; i <observableExperiencia.size(); i++) {
				
				Experiencia experiencia = new Experiencia(observableExperiencia.get(i).getDesde(), observableExperiencia.get(i).getHasta(), observableExperiencia.get(i).getDenominacion(), observableExperiencia.get(i).getEmpleador());
							
				listaExperiencias.add(new ExperienciaBean(experiencia));	
				
				experiencias.add(experiencia);
			}
			
			observableExperiencia = FXCollections.observableArrayList(listaExperiencias);
					
			mainController.getCv().setExperiencias(experiencias);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		
	}
	
	
	/**
	 * Obtiene un objeto CV desde el MainController,
	 * carga los cambios en el objeto CV principal
	 * rellena la lista con los datos
	 * @param cv
	 */
	
	public void cargarElementos(CV cv) {
		
		try {
						
			List<Experiencia> experiencias = cv.getExperiencias(); 
		

			for (int i = 0; i < experiencias.size(); i++) {
				listaExperiencias.add(new ExperienciaBean(experiencias.get(i)));
			}

			observableExperiencia = FXCollections.observableArrayList(listaExperiencias);
			model.setExperienciaList(observableExperiencia);
					
		
		} catch (Exception e) {

		}
				
	}

	// Event Listener on Button[#btnAddExperiencia].onAction
	@FXML
	public void onClickBtnAddExperiencia(ActionEvent event) {

		Experiencia experiencia = dialogNuevaExperiencia();
		
		if(experiencia!=null) {
			listaExperiencias.add(new ExperienciaBean(experiencia));
			observableExperiencia = FXCollections.observableArrayList(listaExperiencias);
			model.setExperienciaList(observableExperiencia);
		}
	}
	// Event Listener on Button[#btnEliminarExperiencia].onAction
	@FXML
	public void onClickBtnEliminarExperiencia(ActionEvent event) {
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Eliminar teléfono");
		alert.setHeaderText("Atención!");
		alert.setContentText("¿Desea eliminar la experiencia " + model.getExperienciaSeleccionada().getDenominacion()+ "?");
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) alert.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == ButtonType.OK) {
			
			model.experienciaListProperty().remove(model.getExperienciaSeleccionada()); //Borra el elemento seleccionado      
			
		} else {
			alert.close();
		}
		
		
		
	}
	
	
	
	

	public TableView<ExperienciaBean> getTableExperiencia() {
		return tableExperiencia;
	}

	public TableColumn<ExperienciaBean, LocalDate> getColumnDesde() {
		return columnDesde;
	}

	public TableColumn<ExperienciaBean, LocalDate> getColumnHasta() {
		return columnHasta;
	}

	public TableColumn<ExperienciaBean, String> getColumnDenominacion() {
		return columnDenominacion;
	}

	public TableColumn<ExperienciaBean, String> getColumnEmpleador() {
		return columnEmpleador;
	}

	public Button getBtnAddExperiencia() {
		return btnAddExperiencia;
	}

	public Button getBtnEliminarExperiencia() {
		return btnEliminarExperiencia;
	}

	public ExperienciaModel getModel() {
		return model;
	}

	public MainController getMainController() {
		return mainController;
	}

	public ArrayList<ExperienciaBean> getListaExperiencias() {
		return listaExperiencias;
	}

	public ObservableList<ExperienciaBean> getObservableExperiencia() {
		return observableExperiencia;
	}

	public GridPane getRoot() {
		return root;
	}

	
	
	
}

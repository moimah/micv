package controller;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import controller.dialogs.FechaDialogController;
import controller.dialogs.NuevoTituloDialogController;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.control.cell.TextFieldTreeTableCell;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.converter.LocalDateStringConverter;
import model.fx.FormacionModel;
import model.fx.beans.FormacionBean;
import model.fx.dialogs.FechaDialogModel;
import model.fx.dialogs.NuevoTituloDialogModel;
import model.pojos.CV;
import model.pojos.Titulo;
import utils.LocalDateAdapter;

public class FormacionController implements Initializable{

	@FXML
	private GridPane root;
	@FXML
	private TableView<FormacionBean> tableFormacion;
	@FXML
	private TableColumn<FormacionBean, LocalDate> columnDesde;
	@FXML
	private TableColumn<FormacionBean, LocalDate> columnHasta;
	@FXML
	private TableColumn<FormacionBean, String> columnDenominacion;
	@FXML
	private TableColumn<FormacionBean, String> columnOrganizador;
	@FXML
	private Button btnAddFormacion;
	@FXML
	private Button btnEliminarFormacion;
	
	
	private FormacionModel model = new FormacionModel(); 
	
	private MainController mainController; 
	
	private ArrayList<FormacionBean> listaFormaciones = new ArrayList<FormacionBean>();
	private ObservableList<FormacionBean> observableTitulo; 
	
	public FormacionController() throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/FormacionView.fxml"));
		loader.setController(this);
		loader.load();		
		
	}
	
	public void injectMainController(MainController mainController) {
		this.mainController = mainController; 
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		
		model.formacionListProperty().bindBidirectional(tableFormacion.itemsProperty());
	    columnDesde.setCellValueFactory(v -> v.getValue().desdeProperty());
	    columnHasta.setCellValueFactory(v -> v.getValue().hastaProperty());
	    columnDenominacion.setCellValueFactory(v -> v.getValue().denominacionProperty());
	    columnOrganizador.setCellValueFactory(v -> v.getValue().organizadorProperty());				
		model.formacionSeleccionadaProperty().bind(tableFormacion.getSelectionModel().selectedItemProperty()); //Bindeo al alemento seleccionado

		model.formacionListProperty().addListener((o, ov, nv)->onChangesDetected(nv));	
		tableFormacion.editingCellProperty().addListener((o, ov, nv)->onChangesDetected(nv));
		
		
		//Hacer la tabla editable
		tableFormacion.setEditable(true);	
		columnDesde.setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateStringConverter())); //Conversor de Date  necesario
		columnHasta.setCellFactory(TextFieldTableCell.forTableColumn(new LocalDateStringConverter()));
		columnDenominacion.setCellFactory(TextFieldTableCell.forTableColumn());
		columnOrganizador.setCellFactory(TextFieldTableCell.forTableColumn());
		
		
			
	}


	/**
	 * Carga un dialogo para añadir nuevo titulo Devuelve un objeto titulo,
	 * null si se ha cancelado 
	 * @return
	 */
	
	public Titulo dialogNuevaFormacion() {
		
		NuevoTituloDialogController dialog = new NuevoTituloDialogController();
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		
		Optional<NuevoTituloDialogModel> result = dialog.showAndWait(); 

				
		if (result.isPresent()) { //Si se han obtenido resultados rellenar objeto telefono	
			Titulo titulo = new Titulo(); 
			titulo.setDenominacion(result.get().getDenominacion());
			titulo.setOrganizador(result.get().getOrganizador());
			titulo.setDesde(result.get().getDesde());
			titulo.setHasta(result.get().getHasta());
			
			return titulo;
		}
		
		return null; 
		
	}
	
	
	/**
	 * @deprecated
	 * Not for the moment
	 * @param fecha
	 * @return
	 */
	public LocalDate dialogNuevaFecha(LocalDate fecha) {
		
		FechaDialogController dialog = new FechaDialogController(); 
		
		dialog.getModel().setFecha(fecha);
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) dialog.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		Optional<FechaDialogModel> result = dialog.showAndWait();

		if (result.isPresent()) { // Si se han obtenido resultados rellenar objeto telefono
			
			LocalDate date = result.get().getFecha();
			
			return date;
		}

		return null;

	}
	
	
	/**
	 * Obtiene un objeto CV desde el MainController,
	 * carga los cambios en el objeto CV principal
	 * rellena la lista con los datos
	 * @param cv
	 */

	public void cargarElementos(CV cv) {
		

		try {
			
			List<Titulo> formacion = cv.getFormacion();
		
			for (int i = 0; i < formacion.size(); i++) {
				listaFormaciones.add(new FormacionBean(formacion.get(i)));
			}

			observableTitulo = FXCollections.observableArrayList(listaFormaciones);
			model.setFormacionList(observableTitulo);
								

		} catch (Exception e) {

		}
		
	}

	
	/**
	 * Reacciona ante los cambios en la lista
	 * Actualiza el objeto principal CV con los nuevos cambios
	 * actualiza el arrayList principal con los cambios
	 * @param nv
	 */	
	public void onChangesDetected(Object nv) {
		
		observableTitulo = model.getFormacionList();		
				
		try {
			List<Titulo> formacion = new ArrayList<Titulo>();
		
			listaFormaciones = new ArrayList<FormacionBean>();	
			
			for (int i = 0; i <observableTitulo.size(); i++) {
						
				Titulo titulo = new Titulo(observableTitulo.get(i).getDesde(), observableTitulo.get(i).getHasta(), observableTitulo.get(i).getDenominacion(), observableTitulo.get(i).getOrganizador());
				listaFormaciones.add(new FormacionBean(titulo));				
				formacion.add(titulo);
			}
			
			observableTitulo = FXCollections.observableArrayList(listaFormaciones);
			
			
			this.mainController.getCv().setFormacion(formacion);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		
	}
	
	

	// Event Listener on Button[#btnAddFormacion].onAction
	@FXML
	public void onClickBtnAddFormacion(ActionEvent event) {
		
		Titulo titulo = dialogNuevaFormacion();
		
		if(titulo!=null) {
			listaFormaciones.add(new FormacionBean(titulo));
			observableTitulo = FXCollections.observableArrayList(listaFormaciones);
			model.setFormacionList(observableTitulo);
		}
		
	}
	
	
	
	// Event Listener on Button[#btnEliminarFormacion].onAction
	@FXML
	public void onClickBtnEliminarFormacion(ActionEvent event) {
		
		
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Eliminar teléfono");
		alert.setHeaderText("Atención!");
		alert.setContentText("¿Desea eliminar la formación " + model.getFormacionSeleccionada().getDenominacion()+ "?");
		
		// Añadido para poner icono
		Stage stage = (Stage) root.getScene().getWindow();
		stage = (Stage) alert.getDialogPane().getScene().getWindow();//
		stage.getIcons().add(new Image("images/cv.png"));
		// Hasta aqui

		Optional<ButtonType> result = alert.showAndWait();

		if (result.get() == ButtonType.OK) {
			
			model.formacionListProperty().remove(model.getFormacionSeleccionada()); //Borra el elemento seleccionado      
			
		} else {
			alert.close();
		}
		
	}
	

	
	
	

	public GridPane getRoot() {
		return root;
	}

	
}

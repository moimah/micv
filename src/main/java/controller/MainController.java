package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.commons.io.FileUtils;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.control.ButtonType;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.pojos.CV;
import utils.JAXBUtil;
import utils.MemoryFiles;

public class MainController implements Initializable  {
	
	@FXML
	private BorderPane root; 
	@FXML
	private MenuItem menuItemNuevo;
	@FXML
	private MenuItem menuItemAbrir;
	@FXML
	private MenuItem menuItemGuardar;
	@FXML
	private MenuItem menuItemGuardarComo;
	@FXML
	private MenuItem onClickMenuItemCerrar;
	@FXML
	private MenuItem menuItemAcercaDe;
	
	@FXML
	private TabPane rootPane;
	@FXML
	private Tab tabPersonal;
	@FXML
	private Tab tabContacto;
	@FXML
	private Tab tabFormacion;
	@FXML
	private Tab tabExperiencia;
	@FXML
	private Tab tabConocimientos;
	
	
	private CV cv = new CV();
	
	
	private File file = new File(""); //Evitamos null pointer al compararlo si no no se ha abierto fichero
	private long oldSize; //Se usara para confirmar el salir sin guardar
	int guardado;
	
	boolean nuevo;
	
	
	//Aqui subcontrollers
	private PersonalController subControllerPersonal;
	private ContactoController subControllerContacto;
	private FormacionController subControllerFormacion;
	private ExperienciaController subControllerExperiencia;
	private ConocimientosController subControllerConocimientos;
	
	
	
	
	//Constructor
	public MainController() throws IOException  {		
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/MainView.fxml"));
		loader.setController(this);
		loader.load();
}
	
	
	
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
			
		
	}
	
	public void cargar() {
		
					// Inicializar subcontroladores
		
				try {					
					
					subControllerPersonal = new PersonalController(); 
					subControllerContacto = new ContactoController();
					subControllerFormacion = new FormacionController();
					subControllerExperiencia = new ExperienciaController();
					subControllerConocimientos = new ConocimientosController();
					
				} catch (Exception e) {
					System.out.println("Error al cargar las vistas");
				}
				
				//Extraer las vistas
				GridPane viewPersonal = subControllerPersonal.getRoot();
				viewPersonal.setAlignment(Pos.CENTER);
				
				GridPane viewContacto = subControllerContacto.getRoot();
				viewContacto.setAlignment(Pos.CENTER);
				
				GridPane viewFormacion = subControllerFormacion.getRoot();
				viewFormacion.setAlignment(Pos.CENTER);
				
				GridPane viewExperiencia = subControllerExperiencia.getRoot();
				viewExperiencia.setAlignment(Pos.CENTER);
				
				GridPane viewConocimientos = subControllerConocimientos.getRoot();
				viewConocimientos.setAlignment(Pos.CENTER);
				
				//Añadir a los tabs
				tabPersonal.setContent(viewPersonal);
				tabContacto.setContent(viewContacto);
				tabFormacion.setContent(viewFormacion);
				tabExperiencia.setContent(viewExperiencia);
				tabConocimientos.setContent(viewConocimientos);
				
				//Inyectarse en los subcontroller
				subControllerPersonal.injectMainController(this);
				subControllerContacto.injectMainController(this);
				subControllerFormacion.injectMainController(this);
				subControllerExperiencia.injectMainController(this);
				subControllerConocimientos.injectMainController(this);
				
		
	}
	/**
	 * File chooser que permite seleccionar ficheros con extensión XML
	 * @return
	 */
	
	public String fileChooserAbrir() {
		
		FileChooser fileChooser = new FileChooser();
		FileChooser.ExtensionFilter fileExtensions = new FileChooser.ExtensionFilter("XML files", "*.xml");
		fileChooser.getExtensionFilters().add(fileExtensions);

		File selectedFile = fileChooser.showOpenDialog(null);
		if (selectedFile != null) {

			String path = selectedFile.getAbsolutePath();

			return path;
		}

		return null;

	}
	
	/**
	 * Dialogo que permite guardar ficheros como,
	 * con extensión por defecto XML
	 */
	
	public void fileChooserGuardarComo()  {
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XML files", "*.xml"));
		fileChooser.setInitialFileName("MiCurriculum.xml");

		file = fileChooser.showSaveDialog(null);

		if (file != null) {		
			
			if(!file.getName().contains(".xml")) { //Nos aseguramos que contenga extension
				  file = new File(file.getAbsolutePath() + ".xml"); //Se le asigna extension xml
			}

				try {
					
					JAXBUtil.save(cv, file); // Guardar el documento
					savelastDocument(file); //Se guarda la ruta en fichero temporal		

					nuevo = false; // El documento deja de ser nuevo
					guardado = 1; //Se pasa a estado guardado
							
					

				} catch (Exception e) {
					e.printStackTrace();
				}

			
		}
		
					

	}
		
	

	// Event Listener on MenuItem[#menuItemNuevo].onAction
	@FXML
	public void onClickMenuItemNuevo(ActionEvent event) {
		
		//Se crea un nuevo CV		
		cv = new CV(); 
		
		//Cargar las vistas		
		cargar();	
		
		//Estado nuevo
		nuevo = true;
			
		
	}
	// Event Listener on MenuItem[#menuItemAbrir].onAction
	@FXML
	public void onClickMenuItemAbrir(ActionEvent event) {
		
		String path = fileChooserAbrir(); 
		
		abrir(path);
		

		
	}
	// Event Listener on MenuItem[#menuItemGuardar].onAction
	@FXML
	public void onClickMenuItemGuardar(ActionEvent event) {
		

		
		if(nuevo==true) { // Se descarta que no sea un fichero nuevo	
			//Si es fichero nuevo se lanza un guardar como					
				
					fileChooserGuardarComo();				
			
			}else { //Si no es fichero nuevo se guarda el la ruta actual		
					
				try {
					JAXBUtil.save(cv, file); //Se guarda en un fichero xml
					
					//Se guarda la ruta en fichero temporal					
					savelastDocument(file);
					
					nuevo = false;
					guardado = 1;
					
					
				} catch (Exception e) {					
					e.printStackTrace();
				}		
			
		}
			
		
	}
	
	
	public void abrir(String path) {

		
		if(path!=null) {
			
						
			file  = new File(path); //Se crea un file con la ruta seleccionada			
			oldSize = file.length(); //Se obtiene el tamaño inicial del fichero
						
			cargar(); //Abrir las vistas		
			
			//Obtener el CV procedente del fichero y cargarlo en las vistas
				
			try {
				
				
			CV cv = JAXBUtil.load(CV.class, file);	//Se lee el xml
			
			//Se guarda la ruta en fichero temporal
			
			savelastDocument(file);
			
			//Se carga el cv el los controladores								
			subControllerPersonal.cargarElementos(cv);
			subControllerContacto.cargarElementos(cv);
			subControllerFormacion.cargarElementos(cv);
			subControllerExperiencia.cargarElementos(cv);
			subControllerConocimientos.cargarElementos(cv);
						
			
			}catch (Exception e) {
				System.out.println("El CV esta vacio");
			}
			
		}
	}
	
	/**
	 * Carga el útimo documento abierto desde un 
	 * fichero temporal .txt
	 */
	
	public void loadLastDocument() {
				
		try {
			
			File temp = new File("temp.txt");
			
			if(temp.exists()) {
				
				String path = MemoryFiles.readFile(temp);		
				File ultimo = new File(path);
				
				if(ultimo.exists()) {
					abrir(path);
				}
			}
								
		}catch (Exception e) {
			//No se ha encontrado el fichero temporal
		}
		
		

	}
	
	/**
	 * Guarda la URI del último documento abierto 
	 * en un fichero temporal .txt
	 * @param file
	 */
	
	public void savelastDocument(File file) {		

		if(file.exists()) {
			MemoryFiles.writeFile(new File("temp.txt"), file.getPath());
		}
				
	}

	
	// Event Listener on MenuItem[#menuItemGuardarComo].onAction
	@FXML
	public void onClickMenuItemGuardarComo(ActionEvent event) {		
	
			fileChooserGuardarComo();						
		
	}
	// Event Listener on MenuItem[#menuItemAcercaDe].onAction
	@FXML
	public void onClickMenuItemAcercaDe(ActionEvent event) {
		// TODO Autogenerated
	}

	// Event Listener on MenuItem[#menuItemSalir].onAction
	@FXML
	public void onClickMenuItemSalir(ActionEvent event) {
		
		salir();
						
	}
	
	/**
	 * Sale del programa, se asegura que se haya 
	 * guardado el documento, si nos e ha guardado,
	 * pregunta mediante un Alert la salida
	 */
	
	public void salir() {

		if (guardado == 0) {

			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Cerrar MiCV");
			alert.setHeaderText("No ha guardado el fichero");
			alert.setContentText("¿Desea salir sin guardar?");
			
			// Añadido para poner icono
			Stage stage = (Stage) root.getScene().getWindow();
			stage = (Stage) alert.getDialogPane().getScene().getWindow();//
			stage.getIcons().add(new Image("images/cv.png"));
			// Hasta aqui

			Optional<ButtonType> result2 = alert.showAndWait();

			if (result2.get() == ButtonType.OK) {

				Platform.exit();

			} else {
				alert.close();
			}

		} else {

			Platform.exit();

		}

	}

		
			



	public BorderPane getRoot() {
		return root;
	}




	public CV getCv() {
		return cv;
	}




	public void setCv(CV cv) {
		this.cv = cv;
	}


	
	


	
	
	
	
}

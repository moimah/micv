package model.pojos;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Telefono {
	
	private String numero; 
	private TipoTelefono tipo;
	
	
	public Telefono() {
		// TODO Auto-generated constructor stub
	}


	public Telefono(String numero, TipoTelefono tipo) {
		super();
		this.numero = numero;
		this.tipo = tipo;
	}

	@XmlAttribute
	public String getNumero() {
		return numero;
	}


	public void setNumero(String numero) {
		this.numero = numero;
	}

	@XmlAttribute
	public TipoTelefono getTipo() {
		return tipo;
	}


	public void setTipo(TipoTelefono tipo) {
		this.tipo = tipo;
	}
	
	
	
	
	

}

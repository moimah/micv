package model.pojos;


import java.util.ArrayList;
import java.util.List;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType
public class Contacto {
	
	private List<Telefono> telefonos = new ArrayList<>(); 
	private List<Email> emails = new ArrayList<>(); 
	private List<Web> webs = new ArrayList<>(); 
	
	
	
	public Contacto() {
		// TODO Auto-generated constructor stub
	}



	public Contacto(List<Telefono> telefonos, List<Email> emails, List<Web> webs) {
		super();
		this.telefonos = telefonos;
		this.emails = emails;
		this.webs = webs;
	}


	@XmlElement
	public List<Telefono> getTelefonos() {
		return telefonos;
	}



	public void setTelefonos(List<Telefono> telefonos) {
		this.telefonos = telefonos;
	}


	@XmlElement
	public List<Email> getEmails() {
		return emails;
	}



	public void setEmails(List<Email> emails) {
		this.emails = emails;
	}


	@XmlElement
	public List<Web> getWebs() {
		return webs;
	}



	public void setWebs(List<Web> webs) {
		this.webs = webs;
	}
	
	
	
	
	
}

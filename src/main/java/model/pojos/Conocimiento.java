package model.pojos;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Conocimiento {
	
		
	private String denominacion;
	private Nivel nivel; 
	

	public Conocimiento() {
		// TODO Auto-generated constructor stub
	}
	
	

	public Conocimiento(String denominacion, Nivel nivel) {
		super();
		this.denominacion = denominacion;
		this.nivel = nivel;
	}



	@XmlAttribute
	public String getDenominacion() {
		return denominacion;
	}


	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}

	@XmlAttribute
	public Nivel getNivel() {
		return nivel;
	}


	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}
	
	
	
}

package model.pojos;



import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Nacionalidad {
	
	private String denominacion;
	
	
	public Nacionalidad() {
		// TODO Auto-generated constructor stub
	}
	
	
	

	public Nacionalidad(String denominacion) {
		super();
		this.denominacion = denominacion;
	}


	
	@XmlAttribute
	public String getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	

}

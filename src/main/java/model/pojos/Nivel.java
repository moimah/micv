package model.pojos;

import java.util.HashMap;
import java.util.Map;

public enum Nivel {
    BASICO(1),
    MEDIO(2),
    AVANZADO(3);

    private int value;
    private static Map map = new HashMap<>();

    private Nivel(int value) {
        this.value = value;
    }

    static {
        for (Nivel nivel : Nivel.values()) {
            map.put(nivel.value, nivel);
        }
    }

    public static Nivel valueOf(int nivel) {
        return (Nivel) map.get(nivel);
    }

    public int getValue() {
        return value;
    }
}

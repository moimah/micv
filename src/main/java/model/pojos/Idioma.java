package model.pojos;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Idioma extends Conocimiento {
		
	
	
	private String certificacion;
	
	public Idioma() {
		// TODO Auto-generated constructor stub
	}
	
	

	public Idioma(String denominacion, Nivel nivel, String certificacion) {
		super(denominacion, nivel);
		this.certificacion = certificacion; 
		
	}


	@XmlAttribute
	public String getCertificacion() {
		return certificacion;
	}

	public void setCertificacion(String certificacion) {
		this.certificacion = certificacion;
	}
	
	

}

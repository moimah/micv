package model.pojos;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlType
@XmlRootElement
public class CV {

	private Personal personal;
	private Contacto contacto;
	private List<Idioma> habilidades = new ArrayList<>();
	private List<Experiencia> experiencias = new ArrayList<>(); 
	private List<Titulo> formacion = new ArrayList<>();
	
	

	
	
	public CV(Personal personal, Contacto contacto, List<Idioma> habilidades, List<Experiencia> experiencias,
			List<Titulo> formacion) {
		super();
		this.personal = personal;
		this.contacto = contacto;
		this.habilidades = habilidades;
		this.experiencias = experiencias;
		this.formacion = formacion;
	}




	@XmlElement
	public Personal getPersonal() {
		return personal;
	}





	public void setPersonal(Personal personal) {
		this.personal = personal;
	}




	@XmlElement
	public Contacto getContacto() {
		return contacto;
	}





	public void setContacto(Contacto contacto) {
		this.contacto = contacto;
	}




	@XmlElement
	public List<Idioma> getHabilidades() {
		return habilidades;
	}





	public void setHabilidades(List<Idioma> habilidades) {
		this.habilidades = habilidades;
	}




	@XmlElement
	public List<Experiencia> getExperiencias() {
		return experiencias;
	}




	
	public void setExperiencias(List<Experiencia> experiencias) {
		this.experiencias = experiencias;
	}




	@XmlElement
	public List<Titulo> getFormacion() {
		return formacion;
	}





	public void setFormacion(List<Titulo> formacion) {
		this.formacion = formacion;
	}





	public CV() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
}

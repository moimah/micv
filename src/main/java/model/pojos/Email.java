package model.pojos;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Email {

	private String direccion;

	public Email() {
		// TODO Auto-generated constructor stub
	}

	public Email(String direccion) {
		super();
		this.direccion = direccion;
	}

	@XmlAttribute
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

}

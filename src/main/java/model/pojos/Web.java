package model.pojos;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Web {

	private String url;
	
	
	public Web() {
		// TODO Auto-generated constructor stub
	}

	public Web(String url) {
		super();
		this.url = url;
	}

	@XmlAttribute
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	} 
	
	
}

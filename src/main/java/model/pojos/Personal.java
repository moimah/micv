package model.pojos;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import utils.LocalDateAdapter;

@XmlType
public class Personal {

	private String identificacion;
	private String nombre; 
	private String apellidos;
	private LocalDate fechaNacimiento;
	private String direccion;
	private String codigoPostal;
	private String localidad;
	private String pais;
	private String dni;
	private List<Nacionalidad> nacionalidades = new ArrayList<>();
	
	
	public Personal() {
		// TODO Auto-generated constructor stub
	}


	public Personal(String identificacion, String nombre, String apellidos, LocalDate fechaNacimiento, String direccion,
			String codigoPostal, String localidad, String pais, String dni, List<Nacionalidad> nacionalidades) {
		super();
		this.identificacion = identificacion;
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.fechaNacimiento = fechaNacimiento;
		this.direccion = direccion;
		this.codigoPostal = codigoPostal;
		this.localidad = localidad;
		this.pais = pais;
		this.dni = dni;
		this.nacionalidades = nacionalidades;
	}

	@XmlAttribute
	public String getIdentificacion() {
		return identificacion;
	}


	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	@XmlElement
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@XmlElement
	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	@XmlElement
	@XmlJavaTypeAdapter(value = LocalDateAdapter.class)
	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	@XmlElement
	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	@XmlElement
	public String getCodigoPostal() {
		return codigoPostal;
	}


	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@XmlElement
	public String getLocalidad() {
		return localidad;
	}


	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	@XmlElement
	public String getPais() {
		return pais;
	}


	public void setPais(String pais) {
		this.pais = pais;
	}

	@XmlElement
	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}

	@XmlElement
	public List<Nacionalidad> getNacionalidades() {
		return nacionalidades;
	}


	public void setNacionalidades(List<Nacionalidad> nacionalidades) {
		this.nacionalidades = nacionalidades;
	}
		
	
	
	
}

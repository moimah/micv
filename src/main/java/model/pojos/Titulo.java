package model.pojos;

import java.time.LocalDate;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import utils.LocalDateAdapter;

@XmlType
public class Titulo {
	
	private LocalDate desde; 
	private LocalDate hasta;
	private String denominacion;
	private String organizador;
	
	
	public Titulo() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	public Titulo(LocalDate desde, LocalDate hasta, String denominacion, String organizador) {
		super();
		this.desde = desde;
		this.hasta = hasta;
		this.denominacion = denominacion;
		this.organizador = organizador;
	}



	@XmlAttribute
	@XmlJavaTypeAdapter(value = LocalDateAdapter.class)
	public LocalDate getDesde() {
		return desde;
	}
	public void setDesde(LocalDate desde) {
		this.desde = desde;
	}
	
	@XmlAttribute
	@XmlJavaTypeAdapter(value = LocalDateAdapter.class)
	public LocalDate getHasta() {
		return hasta;
	}
	public void setHasta(LocalDate hasta) {
		this.hasta = hasta;
	}
	
	@XmlElement
	public String getDenominacion() {
		return denominacion;
	}
	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}
	@XmlElement
	public String getOrganizador() {
		return organizador;
	}
	public void setOrganizador(String organizador) {
		this.organizador = organizador;
	}
	
	
	
	

}

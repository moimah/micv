package model.fx;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import model.fx.beans.HabilidadesBean;

public class ConocimientosModel {

	private ListProperty<HabilidadesBean> listaConocimientos = new SimpleListProperty<HabilidadesBean>(); 
	private ObjectProperty<HabilidadesBean> conocimientoSeleccionado = new SimpleObjectProperty<HabilidadesBean>();
	public final ListProperty<HabilidadesBean> listaConocimientosProperty() {
		return this.listaConocimientos;
	}
	
	public final ObservableList<HabilidadesBean> getListaConocimientos() {
		return this.listaConocimientosProperty().get();
	}
	
	public final void setListaConocimientos(final ObservableList<HabilidadesBean> listaConocimientos) {
		this.listaConocimientosProperty().set(listaConocimientos);
	}
	
	public final ObjectProperty<HabilidadesBean> conocimientoSeleccionadoProperty() {
		return this.conocimientoSeleccionado;
	}
	
	public final HabilidadesBean getConocimientoSeleccionado() {
		return this.conocimientoSeleccionadoProperty().get();
	}
	
	public final void setConocimientoSeleccionado(final HabilidadesBean conocimientoSeleccionado) {
		this.conocimientoSeleccionadoProperty().set(conocimientoSeleccionado);
	}
	 	
	
}

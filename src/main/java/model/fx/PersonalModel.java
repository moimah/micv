package model.fx;

import java.time.LocalDate;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.pojos.Nacionalidad;

public class PersonalModel {
	
	private StringProperty txtNombre = new SimpleStringProperty();
	private StringProperty txtDni = new SimpleStringProperty();
	private StringProperty txtApellidos = new SimpleStringProperty();
	private StringProperty txtDireccion = new SimpleStringProperty();
	private ObjectProperty<LocalDate> dateFechaNacimiento = new SimpleObjectProperty<>(); //Probando el bindeo de un localDate
	private StringProperty txtCodigoPostal = new SimpleStringProperty();
	private StringProperty txtLocalidad = new SimpleStringProperty();	
	private ListProperty<String> cmbPais = new SimpleListProperty<String>(FXCollections.observableArrayList());	
	private StringProperty paisSeleccionado = new SimpleStringProperty();
	private ListProperty<String> listNacionalidad = new SimpleListProperty<String>(FXCollections.observableArrayList());
		
	private StringProperty nacionalidadSelecciona = new SimpleStringProperty();

	public final StringProperty txtNombreProperty() {
		return this.txtNombre;
	}
	

	public final String getTxtNombre() {
		return this.txtNombreProperty().get();
	}
	

	public final void setTxtNombre(final String txtNombre) {
		this.txtNombreProperty().set(txtNombre);
	}
	

	public final StringProperty txtDniProperty() {
		return this.txtDni;
	}
	

	public final String getTxtDni() {
		return this.txtDniProperty().get();
	}
	

	public final void setTxtDni(final String txtDni) {
		this.txtDniProperty().set(txtDni);
	}
	

	public final StringProperty txtApellidosProperty() {
		return this.txtApellidos;
	}
	

	public final String getTxtApellidos() {
		return this.txtApellidosProperty().get();
	}
	

	public final void setTxtApellidos(final String txtApellidos) {
		this.txtApellidosProperty().set(txtApellidos);
	}
	

	public final StringProperty txtDireccionProperty() {
		return this.txtDireccion;
	}
	

	public final String getTxtDireccion() {
		return this.txtDireccionProperty().get();
	}
	

	public final void setTxtDireccion(final String txtDireccion) {
		this.txtDireccionProperty().set(txtDireccion);
	}
	

	public final ObjectProperty<LocalDate> dateFechaNacimientoProperty() {
		return this.dateFechaNacimiento;
	}
	

	public final LocalDate getDateFechaNacimiento() {
		return this.dateFechaNacimientoProperty().get();
	}
	

	public final void setDateFechaNacimiento(final LocalDate dateFechaNacimiento) {
		this.dateFechaNacimientoProperty().set(dateFechaNacimiento);
	}
	

	public final StringProperty txtCodigoPostalProperty() {
		return this.txtCodigoPostal;
	}
	

	public final String getTxtCodigoPostal() {
		return this.txtCodigoPostalProperty().get();
	}
	

	public final void setTxtCodigoPostal(final String txtCodigoPostal) {
		this.txtCodigoPostalProperty().set(txtCodigoPostal);
	}
	

	public final StringProperty txtLocalidadProperty() {
		return this.txtLocalidad;
	}
	

	public final String getTxtLocalidad() {
		return this.txtLocalidadProperty().get();
	}
	

	public final void setTxtLocalidad(final String txtLocalidad) {
		this.txtLocalidadProperty().set(txtLocalidad);
	}
	

	public final ListProperty<String> cmbPaisProperty() {
		return this.cmbPais;
	}
	

	public final ObservableList<String> getCmbPais() {
		return this.cmbPaisProperty().get();
	}
	

	public final void setCmbPais(final ObservableList<String> cmbPais) {
		this.cmbPaisProperty().set(cmbPais);
	}
	

	public final StringProperty paisSeleccionadoProperty() {
		return this.paisSeleccionado;
	}
	

	public final String getPaisSeleccionado() {
		return this.paisSeleccionadoProperty().get();
	}
	

	public final void setPaisSeleccionado(final String paisSeleccionado) {
		this.paisSeleccionadoProperty().set(paisSeleccionado);
	}
	

	public final ListProperty<String> listNacionalidadProperty() {
		return this.listNacionalidad;
	}
	

	public final ObservableList<String> getListNacionalidad() {
		return this.listNacionalidadProperty().get();
	}
	

	public final void setListNacionalidad(final ObservableList<String> listNacionalidad) {
		this.listNacionalidadProperty().set(listNacionalidad);
	}
	

	public final StringProperty nacionalidadSeleccionaProperty() {
		return this.nacionalidadSelecciona;
	}
	

	public final String getNacionalidadSelecciona() {
		return this.nacionalidadSeleccionaProperty().get();
	}
	

	public final void setNacionalidadSelecciona(final String nacionalidadSelecciona) {
		this.nacionalidadSeleccionaProperty().set(nacionalidadSelecciona);
	}
	 


}

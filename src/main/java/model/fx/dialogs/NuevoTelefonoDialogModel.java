package model.fx.dialogs;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class NuevoTelefonoDialogModel {
	
	private StringProperty numero = new SimpleStringProperty(); 
	private ListProperty<String> tipoTelefono = new SimpleListProperty<String>();
	private StringProperty seleccionado = new SimpleStringProperty();
	public final StringProperty numeroProperty() {
		return this.numero;
	}
	
	public final String getNumero() {
		return this.numeroProperty().get();
	}
	
	public final void setNumero(final String numero) {
		this.numeroProperty().set(numero);
	}
	
	public final ListProperty<String> tipoTelefonoProperty() {
		return this.tipoTelefono;
	}
	
	public final ObservableList<String> getTipoTelefono() {
		return this.tipoTelefonoProperty().get();
	}
	
	public final void setTipoTelefono(final ObservableList<String> tipoTelefono) {
		this.tipoTelefonoProperty().set(tipoTelefono);
	}
	
	public final StringProperty seleccionadoProperty() {
		return this.seleccionado;
	}
	
	public final String getSeleccionado() {
		return this.seleccionadoProperty().get();
	}
	
	public final void setSeleccionado(final String seleccionado) {
		this.seleccionadoProperty().set(seleccionado);
	}

	@Override
	public String toString() {
		return "NuevoTelefonoDialogModel [numero=" + numero + ", tipoTelefono=" + tipoTelefono + ", seleccionado="
				+ seleccionado + "]";
	}
	
	
	


}

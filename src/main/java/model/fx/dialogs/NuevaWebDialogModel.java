package model.fx.dialogs;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class NuevaWebDialogModel {
	
	private StringProperty url = new SimpleStringProperty();

	public final StringProperty urlProperty() {
		return this.url;
	}
	

	public final String getUrl() {
		return this.urlProperty().get();
	}
	

	public final void setUrl(final String url) {
		this.urlProperty().set(url);
	}


	@Override
	public String toString() {
		return "NuevaWebDialogModel [url=" + url + "]";
	}
	
	
	

}

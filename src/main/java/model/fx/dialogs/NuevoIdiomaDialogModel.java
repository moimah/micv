package model.fx.dialogs;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;

public class NuevoIdiomaDialogModel {
	
	private StringProperty denominacion = new SimpleStringProperty();
	private ListProperty<String> nivel = new SimpleListProperty<String>();
	private StringProperty nivelSeleccionado = new SimpleStringProperty();
	private StringProperty certificacion = new SimpleStringProperty();
	public final StringProperty denominacionProperty() {
		return this.denominacion;
	}
	
	public final String getDenominacion() {
		return this.denominacionProperty().get();
	}
	
	public final void setDenominacion(final String denominacion) {
		this.denominacionProperty().set(denominacion);
	}
	
	public final ListProperty<String> nivelProperty() {
		return this.nivel;
	}
	
	public final ObservableList<String> getNivel() {
		return this.nivelProperty().get();
	}
	
	public final void setNivel(final ObservableList<String> nivel) {
		this.nivelProperty().set(nivel);
	}
	
	public final StringProperty nivelSeleccionadoProperty() {
		return this.nivelSeleccionado;
	}
	
	public final String getNivelSeleccionado() {
		return this.nivelSeleccionadoProperty().get();
	}
	
	public final void setNivelSeleccionado(final String nivelSeleccionado) {
		this.nivelSeleccionadoProperty().set(nivelSeleccionado);
	}
	
	public final StringProperty certificacionProperty() {
		return this.certificacion;
	}
	
	public final String getCertificacion() {
		return this.certificacionProperty().get();
	}
	
	public final void setCertificacion(final String certificacion) {
		this.certificacionProperty().set(certificacion);
	}
	 
	
	

}

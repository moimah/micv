package model.fx.dialogs;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class NuevoCorreoDialogModel {
	
	private StringProperty email = new SimpleStringProperty();

	public final StringProperty emailProperty() {
		return this.email;
	}
	

	public final String getEmail() {
		return this.emailProperty().get();
	}
	

	public final void setEmail(final String email) {
		this.emailProperty().set(email);
	}


	@Override
	public String toString() {
		return "NuevoCorreoDialogModel [email=" + email + "]";
	}
	
	
	

}

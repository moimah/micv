package model.fx.dialogs;

import java.time.LocalDate;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class FechaDialogModel {
	
	private ObjectProperty<LocalDate> fecha = new SimpleObjectProperty<LocalDate>();

	public final ObjectProperty<LocalDate> fechaProperty() {
		return this.fecha;
	}
	

	public final LocalDate getFecha() {
		return this.fechaProperty().get();
	}
	

	public final void setFecha(final LocalDate fecha) {
		this.fechaProperty().set(fecha);
	}
	
	
	

}

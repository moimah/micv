package model.fx;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.fx.beans.EmailBean;
import model.fx.beans.TelefonoBean;
import model.fx.beans.WebBean;

public class ContactoModel {
	
	private ListProperty<TelefonoBean> telefonos = new SimpleListProperty<TelefonoBean>(FXCollections.observableArrayList());
	private ObjectProperty<TelefonoBean> telefonoSeleccionado = new SimpleObjectProperty<TelefonoBean>(); //Prueba para elemento seleccionado
	
	private ListProperty<EmailBean> emails  = new SimpleListProperty<EmailBean>(FXCollections.observableArrayList());
	private ObjectProperty<EmailBean> emailSeleccionado = new SimpleObjectProperty<EmailBean>(); //Prueba para elemento seleccionado	
	
	private ListProperty<WebBean> webs = new SimpleListProperty<WebBean>(FXCollections.observableArrayList());
    private ObjectProperty<WebBean> webSeleccionada = new SimpleObjectProperty<WebBean>(); //Prueba para elemento seleccionado
    
    
	public final ListProperty<TelefonoBean> telefonosProperty() {
		return this.telefonos;
	}
	
	public final ObservableList<TelefonoBean> getTelefonos() {
		return this.telefonosProperty().get();
	}
	
	public final void setTelefonos(final ObservableList<TelefonoBean> telefonos) {
		this.telefonosProperty().set(telefonos);
	}
	
	public final ObjectProperty<TelefonoBean> telefonoSeleccionadoProperty() {
		return this.telefonoSeleccionado;
	}
	
	public final TelefonoBean getTelefonoSeleccionado() {
		return this.telefonoSeleccionadoProperty().get();
	}
	
	public final void setTelefonoSeleccionado(final TelefonoBean telefonoSeleccionado) {
		this.telefonoSeleccionadoProperty().set(telefonoSeleccionado);
	}
	
	public final ListProperty<EmailBean> emailsProperty() {
		return this.emails;
	}
	
	public final ObservableList<EmailBean> getEmails() {
		return this.emailsProperty().get();
	}
	
	public final void setEmails(final ObservableList<EmailBean> emails) {
		this.emailsProperty().set(emails);
	}
	
	public final ObjectProperty<EmailBean> emailSeleccionadoProperty() {
		return this.emailSeleccionado;
	}
	
	public final EmailBean getEmailSeleccionado() {
		return this.emailSeleccionadoProperty().get();
	}
	
	public final void setEmailSeleccionado(final EmailBean emailSeleccionado) {
		this.emailSeleccionadoProperty().set(emailSeleccionado);
	}
	
	public final ListProperty<WebBean> websProperty() {
		return this.webs;
	}
	
	public final ObservableList<WebBean> getWebs() {
		return this.websProperty().get();
	}
	
	public final void setWebs(final ObservableList<WebBean> webs) {
		this.websProperty().set(webs);
	}
	
	public final ObjectProperty<WebBean> webSeleccionadaProperty() {
		return this.webSeleccionada;
	}
	
	public final WebBean getWebSeleccionada() {
		return this.webSeleccionadaProperty().get();
	}
	
	public final void setWebSeleccionada(final WebBean webSeleccionada) {
		this.webSeleccionadaProperty().set(webSeleccionada);
	}
		

}

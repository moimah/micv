package model.fx.beans;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.pojos.Idioma;
import model.pojos.Nivel;
import model.pojos.TipoTelefono;

public class HabilidadesBean {
	
	private StringProperty denominacion = new SimpleStringProperty(); 
	private StringProperty certificacion = new SimpleStringProperty(); 
	private ObjectProperty<Nivel> nivel = new SimpleObjectProperty<Nivel>();
	
	private Idioma habilidad = new Idioma(); 
	
	public HabilidadesBean(Idioma habilidad) {
		
		this.habilidad = habilidad; 
		denominacion.set(habilidad.getDenominacion());
		certificacion.set(habilidad.getCertificacion());
				
		nivel.set(habilidad.getNivel());
		
	}

	public final StringProperty denominacionProperty() {
		return this.denominacion;
	}
	

	public final String getDenominacion() {
		return this.denominacionProperty().get();
	}
	

	public final void setDenominacion(final String denominacion) {
		this.denominacionProperty().set(denominacion);
	}
	

	public final StringProperty certificacionProperty() {
		return this.certificacion;
	}
	

	public final String getCertificacion() {
		return this.certificacionProperty().get();
	}
	

	public final void setCertificacion(final String certificacion) {
		this.certificacionProperty().set(certificacion);
	}
	

	public final ObjectProperty<Nivel> nivelProperty() {
		return this.nivel;
	}
	

	public final Nivel getNivel() {
		return this.nivelProperty().get();
	}
	

	public final void setNivel(final Nivel nivel) {
		this.nivelProperty().set(nivel);
	}

	public Idioma getHabilidad() {
		return habilidad;
	}
	
	
	
}

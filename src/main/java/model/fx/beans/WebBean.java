package model.fx.beans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.pojos.Web;

public class WebBean {
	
	private StringProperty url = new SimpleStringProperty();
	
	private Web web;
	
	public WebBean(Web web) {
		this.web = web; 
		url.set(web.getUrl());
	}

	public final StringProperty urlProperty() {
		return this.url;
	}
	

	public final String getUrl() {
		return this.urlProperty().get();
	}
	

	public final void setUrl(final String url) {
		this.urlProperty().set(url);
	}

	public Web getWeb() {
		return web;
	}
	
	
	

}

package model.fx.beans;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.pojos.Email;

public class EmailBean {
	
	private StringProperty direccion = new SimpleStringProperty();
	
	private Email email;
	
	public EmailBean(Email email) {
		this.email = email; 
		direccion.set(email.getDireccion());		
	}

	public final StringProperty direccionProperty() {
		return this.direccion;
	}
	

	public final String getDireccion() {
		return this.direccionProperty().get();
	}
	

	public final void setDireccion(final String direccion) {
		this.direccionProperty().set(direccion);
	}

	public Email getEmail() {
		return email;
	}
	
	
	
	

}

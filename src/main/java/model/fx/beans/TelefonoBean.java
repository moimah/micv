package model.fx.beans;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.pojos.Telefono;
import model.pojos.TipoTelefono;

public class TelefonoBean {
	
	private StringProperty numero = new SimpleStringProperty();
	private ObjectProperty<TipoTelefono> tipoTelefono = new SimpleObjectProperty<TipoTelefono>();
	
	private Telefono telefono;
	
	public TelefonoBean(Telefono telefono) {
		this.telefono = telefono;
		numero.set(telefono.getNumero());
		tipoTelefono.set(telefono.getTipo());
		
		
	}

	public final StringProperty numeroProperty() {
		return this.numero;
	}
	

	public final String getNumero() {
		return this.numeroProperty().get();
	}
	

	public final void setNumero(final String numero) {
		this.numeroProperty().set(numero);
	}
	

	public final ObjectProperty<TipoTelefono> tipoTelefonoProperty() {
		return this.tipoTelefono;
	}
	

	public final TipoTelefono getTipoTelefono() {
		return this.tipoTelefonoProperty().get();
	}
	

	public final void setTipoTelefono(final TipoTelefono tipoTelefono) {
		this.tipoTelefonoProperty().set(tipoTelefono);
	}

	public Telefono getTelefono() {
		return telefono;
	}

		

}

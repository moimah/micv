package model.fx;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import model.fx.beans.ExperienciaBean;

public class ExperienciaModel {
	
	private ListProperty<ExperienciaBean> experienciaList = new SimpleListProperty<ExperienciaBean>();
	private ObjectProperty<ExperienciaBean> experienciaSeleccionada = new SimpleObjectProperty<ExperienciaBean>();
	public final ListProperty<ExperienciaBean> experienciaListProperty() {
		return this.experienciaList;
	}
	
	public final ObservableList<ExperienciaBean> getExperienciaList() {
		return this.experienciaListProperty().get();
	}
	
	public final void setExperienciaList(final ObservableList<ExperienciaBean> experienciaList) {
		this.experienciaListProperty().set(experienciaList);
	}
	
	public final ObjectProperty<ExperienciaBean> experienciaSeleccionadaProperty() {
		return this.experienciaSeleccionada;
	}
	
	public final ExperienciaBean getExperienciaSeleccionada() {
		return this.experienciaSeleccionadaProperty().get();
	}
	
	public final void setExperienciaSeleccionada(final ExperienciaBean experienciaSeleccionada) {
		this.experienciaSeleccionadaProperty().set(experienciaSeleccionada);
	}
	 
	
	

}

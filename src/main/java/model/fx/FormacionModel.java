package model.fx;

import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import model.fx.beans.FormacionBean;

public class FormacionModel {
	
	private ListProperty<FormacionBean> formacionList = new SimpleListProperty<FormacionBean>();
	private ObjectProperty<FormacionBean> formacionSeleccionada = new SimpleObjectProperty<FormacionBean>();
	public final ListProperty<FormacionBean> formacionListProperty() {
		return this.formacionList;
	}
	
	public final ObservableList<FormacionBean> getFormacionList() {
		return this.formacionListProperty().get();
	}
	
	public final void setFormacionList(final ObservableList<FormacionBean> formacionList) {
		this.formacionListProperty().set(formacionList);
	}
	
	public final ObjectProperty<FormacionBean> formacionSeleccionadaProperty() {
		return this.formacionSeleccionada;
	}
	
	public final FormacionBean getFormacionSeleccionada() {
		return this.formacionSeleccionadaProperty().get();
	}
	
	public final void setFormacionSeleccionada(final FormacionBean formacionSeleccionada) {
		this.formacionSeleccionadaProperty().set(formacionSeleccionada);
	}
	 
	
	

}

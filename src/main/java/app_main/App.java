package app_main;


import controller.MainController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class App extends Application {
	
	private MainController controller; 
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		MainController controller = new MainController();		
		
		Scene scene = new Scene(controller.getRoot(), 800, 600);				
		
		primaryStage.setTitle("MiCV");
		primaryStage.setScene(scene);
		//primaryStage.getIcons().add(new Image("/images/cv.png"));
		primaryStage.setMinHeight(600);
		primaryStage.setMinWidth(800);
		primaryStage.show();
		
		controller.loadLastDocument();
		

	}
	
	

	public static void main(String[] args) {
		launch(args);

	}

}
